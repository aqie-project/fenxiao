package com.pojoin.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.pojoin.system.dao.ShopchanneluserDao;
import com.pojoin.system.domain.ShopchanneluserDO;
import com.pojoin.system.service.ShopchanneluserService;



@Service
public class ShopchanneluserServiceImpl implements ShopchanneluserService {
	@Autowired
	private ShopchanneluserDao shopchanneluserDao;
	
	@Override
	public ShopchanneluserDO get(Integer id){
		return shopchanneluserDao.get(id);
	}
	
	@Override
	public List<ShopchanneluserDO> list(Map<String, Object> map){
		return shopchanneluserDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return shopchanneluserDao.count(map);
	}
	
	@Override
	public int save(ShopchanneluserDO shopchanneluser){
		return shopchanneluserDao.save(shopchanneluser);
	}
	
	@Override
	public int update(ShopchanneluserDO shopchanneluser){
		return shopchanneluserDao.update(shopchanneluser);
	}
	
	@Override
	public int remove(Integer id){
		return shopchanneluserDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return shopchanneluserDao.batchRemove(ids);
	}
	
}
