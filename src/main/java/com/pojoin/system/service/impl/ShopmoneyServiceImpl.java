package com.pojoin.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.pojoin.system.dao.ShopmoneyDao;
import com.pojoin.system.domain.ShopmoneyDO;
import com.pojoin.system.service.ShopmoneyService;



@Service
public class ShopmoneyServiceImpl implements ShopmoneyService {
	@Autowired
	private ShopmoneyDao shopmoneyDao;
	
	@Override
	public ShopmoneyDO get(Integer id){
		return shopmoneyDao.get(id);
	}
	
	@Override
	public List<ShopmoneyDO> list(Map<String, Object> map){
		return shopmoneyDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return shopmoneyDao.count(map);
	}
	
	@Override
	public int save(ShopmoneyDO shopmoney){
		return shopmoneyDao.save(shopmoney);
	}
	
	@Override
	public int update(ShopmoneyDO shopmoney){
		return shopmoneyDao.update(shopmoney);
	}
	
	@Override
	public int remove(Integer id){
		return shopmoneyDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return shopmoneyDao.batchRemove(ids);
	}
	
}
