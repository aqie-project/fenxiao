package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户现金表
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-16 14:12:57
 */
public class ShopmoneyDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//
	private Date createtime;
	//
	private Integer userid;
	//
	private Double money;
	//
	private Double coupon;
	//
	private Double charge;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * 获取：
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * 设置：
	 */
	public void setCoupon(Double coupon) {
		this.coupon = coupon;
	}
	/**
	 * 获取：
	 */
	public Double getCoupon() {
		return coupon;
	}
	/**
	 * 设置：
	 */
	public void setCharge(Double charge) {
		this.charge = charge;
	}
	/**
	 * 获取：
	 */
	public Double getCharge() {
		return charge;
	}
}
