package com.pojoin.system.controller;

import com.pojoin.common.annotation.Log;
import com.pojoin.common.controller.BaseController;
import com.pojoin.common.utils.PageUtils;
import com.pojoin.common.utils.Query;
import com.pojoin.common.utils.R;
import com.pojoin.system.dao.TixianDao;
import com.pojoin.system.dao.UserDao;
import com.pojoin.system.dao.UserRoleDao;
import com.pojoin.system.domain.TixianDO;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.service.MoneyHistoryService;
import com.pojoin.system.service.TixianService;
import com.pojoin.system.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 提现表
 *
 * @author pojoin
 * @email
 * @date 2020-06-30 19:43:49
 */

@Controller
@RequestMapping("/system/tixian")
public class TixianController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TixianService tixianService;
	@Autowired
	UserRoleDao userRoleDao;
	@Autowired
	MoneyHistoryService moneyHistoryService;
	@GetMapping()
	String Tixian(Model model){
		model.addAttribute("user", userRoleDao.getRoleId(userService.get(getUserId()).getUserId()).getRoleId());
	    return "system/tixian/tixian";
	}

	@ResponseBody
	@GetMapping("/list")
	@Log("提现列表")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
		if(getUserId() == 149 || getUserId() == 148){
			params.remove("userId");
		}else{
			params.put("userId", getUserId());
		}
        Query query = new Query(params);
		List<TixianDO> tixianList = tixianService.list(query);
		int total = tixianService.count(query);
		PageUtils pageUtils = new PageUtils(tixianList, total);
		return pageUtils;
	}

	@GetMapping("/add")
	String add(Model model){
		UserDO userDO=userService.get(getUserId());
		model.addAttribute("user", userDO);
	    return "system/tixian/add";
	}

	/**
	 * 修改提现状态
	 * @param id
	 * @return
	 */
	@PostMapping("/update")
	@ResponseBody
	R update(Integer id) {
		TixianDO t=new TixianDO();
		t.setId(id);
		t.setStatus("1");
		t.setPayTime(new Date());
		tixianService.update(t);
		return R.ok();
	}

    /**
     * 驳回订单
     * @param id
     * @return
     */
    @PostMapping("/bohui")
    @ResponseBody
	@Log("提现驳回")
    R bohui(TixianDO tixianDO) {
        TixianDO t=tixianService.get(tixianDO.getId());
        Double d=Double.valueOf(t.getMoney())/0.97d;
        t.setMoney(String.valueOf(d));
        UserDO userDO=userService.get(Long.valueOf(t.getUserId()));
        userDO.setMoney(userDO.getMoney()+Double.valueOf(t.getMoney()));
        userService.update(userDO);
        t.setBank1(tixianDO.getBank1());
        t.setStatus("2");
        tixianService.update(t);
        return R.ok();
    }

	@GetMapping("/edit/{id}")
	String edit(@PathVariable("id") Integer id,Model model){
		TixianDO tixian = tixianService.get(id);
		model.addAttribute("tixian", tixian);
	    return "system/tixian/edit";
	}
	@Autowired
	UserService userService;

	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@Log("提现")
	public R save( TixianDO tixian){

		if(!isNumber(tixian.getMoney())){
			return R.error("请输入合法金额");
		}

		if(userService.get(getUserId()).getMoney()<Double.valueOf(tixian.getMoney()) ){
			return R.error("余额不足！");
		}
		tixian.setCreatTime(new Date());
		tixian.setStatus("0");
		tixian.setUserId(Integer.valueOf(getUserId().toString()));
		if(tixianService.save(tixian)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 发放现金，修改用户提现状态
	 */
	@ResponseBody
	@RequestMapping("/update")
	@Log("发放")
	public R update( TixianDO tixian){
		tixian.setPayTime(new Date());
		tixianService.update(tixian);
		return R.ok();
	}
	@Autowired
	UserDao userDao;
	@Autowired
	TixianDao tixianDao;

	/**
	 * @param userId  用户id
	 * @param money   提现金额
	 * @param account 支付宝账号
	 * @param name    姓名
	 * @param remark  备注
	 * @param type    1:微信；2：支付宝
	 * @return
	 * @throws Exception
	 */



	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	public R remove( Integer id){
		if(tixianService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	public R remove(@RequestParam("ids[]") Integer[] ids){
		tixianService.batchRemove(ids);
		return R.ok();
	}

	public static boolean isNumber(String str) {
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$"); // 判断小数点后2位的数字的正则表达式
		java.util.regex.Matcher match = pattern.matcher(str);
		if (match.matches() == false) {
			return false;
		} else {
			return true;
		}
	}


}
