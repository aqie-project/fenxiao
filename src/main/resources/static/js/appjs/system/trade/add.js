$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/system/trade/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			bank2 : {
				required : true
			},
            money: {
                required: true
            }
		},
		messages : {
            bank2 : {
				required : icon + "请输入编号"
			},
            money: {
                required: icon + "请输入金额"
            }
		}
	})
}

$(function () {
    $("#bank2").bind('input propertychange', function () {
        var _this = $("#bank2").val();
        $.ajax({
            type: "POST",
            url: "/sys/mem/getnameBycode",
            data: {invitateCode: _this},
            success: function (data) {
                console.log(JSON.stringify(data))
                if (data.code == 0) {
                    $("#tname").text(data.msg);
                    $("#showname1").show();
                }
            }
        });
    })
})