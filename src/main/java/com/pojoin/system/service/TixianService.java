package com.pojoin.system.service;

import com.pojoin.system.domain.TixianDO;

import java.util.List;
import java.util.Map;

/**
 * 提现表
 * 
 * @author pojoin
 * @email 
 * @date 2020-06-30 19:43:49
 */
public interface TixianService {
	
	TixianDO get(Integer id);
	
	List<TixianDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(TixianDO tixian);
	
	int update(TixianDO tixian);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
