package com.pojoin.system.dao;

import com.pojoin.system.domain.ShopmoneyDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 用户现金表
 * @author pojoin
 * @email 
 * @date 2020-07-16 14:12:57
 */
@Mapper
public interface ShopmoneyDao {

	ShopmoneyDO get(Integer id);
	
	List<ShopmoneyDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ShopmoneyDO shopmoney);
	
	int update(ShopmoneyDO shopmoney);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
