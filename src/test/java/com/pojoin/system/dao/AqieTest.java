package com.pojoin.system.dao;

import com.pojoin.system.domain.GradeDO;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.domain.vo.User;
import com.pojoin.system.domain.vo.UserGradeVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: aqie
 * @create: 2020-10-11 16:55
 **/
//@SpringBootTest
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
@Slf4j
public class AqieTest {
    @Autowired
    UserDao userDao;

    @Autowired
    private GradeDao gradeDao;

    @Autowired
    UserDao userMapper;
    /**
     * UserDO{userId=38366, username='13315868505', name='李红霞', password='de54a7fb0c5113609f7766286a18f8f0', deptId=89, deptName='null', email='', mobile='13315868505', status=1, userIdCreate=null, gmtCreate=null, gmtModified=Wed Jul 29 09:57:59 CST 2020, roleIds=null, sex=1, birth=null, picId=null, liveAddress='', hobby='', province='null', city='null', district=''},
     */

    /**
     * 测试用户 user_id 39150 用户星级 4星
     *
     */
    // @Test
    public void test(){
        log.info("aqie test");
        List<User> inviteList = userDao.listInvite2("0,149,25367,27285,28802,38366");
        for (User user: inviteList){
            if (user != null) {
                Long userId = user.getUserId();
                // 1. 查询用户星级
                log.info("查询用户星级 {} {} {}", userId,user.getGrade(),user.getParentId());
                // 查询用户百分比
                Double proportion = gradeDao.getByGrade(user.getGrade()).getProportion();
                // 2. 查询用户父亲星级及百分比
                UserGradeVo userGradeVo = userDao.selectUserGradeByUserId(user.getParentId());
                log.info("查询用户父亲星级及百分比 {} {}",userGradeVo.getGrade(),userGradeVo.getProportion());
            }

        }

        Assert.assertEquals(5, inviteList.size());

        Map map = new HashMap<>();
        map.put("grade", "五星");
        List<UserDO> userDOS = userDao.listAll(map);
        // log.info("列表 {}", userDOS);


        List<GradeDO> list = gradeDao.list(new HashMap<>());

        // log.info("列表 {}", list);
    }

    // @Test
    public void test2(){
        UserDO aDo = userMapper.get(25367L);
        aDo.setGrade("一星");
        int update = userMapper.updateGrade("一星", aDo.getUserId());
        log.info("更新结果 {}", update);
    }
    // @Test
    public void test3(){
        int i = 0;
        int b = ++i;
        System.out.println(b); // 0
    }

    // @Test
    public void test4() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("sort", "id");
        map.put("order", "asc");
        List<GradeDO> gradeList = gradeDao.list(map);
        log.info("{}", gradeList);
    }
}
