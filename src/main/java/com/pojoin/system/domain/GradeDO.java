package com.pojoin.system.domain;

import java.io.Serializable;



/**
 * 
 * 
 * @author pojoin
 * @email 
 * @date 2020-06-30 16:25:00
 */
public class GradeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//等级名称
	private String gradeName;
	//数量
	private Integer number;
	//
	private Double proportion;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：等级名称
	 */
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	/**
	 * 获取：等级名称
	 */
	public String getGradeName() {
		return gradeName;
	}
	/**
	 * 设置：数量
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}
	/**
	 * 获取：数量
	 */
	public Integer getNumber() {
		return number;
	}
	/**
	 * 设置：
	 */
	public void setProportion(Double proportion) {
		this.proportion = proportion;
	}
	/**
	 * 获取：
	 */
	public Double getProportion() {
		return proportion;
	}
}
