package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 充值表
 * 
 * @author pojoin
 * @email 
 * @date 2020-06-30 20:02:39
 */
public class OrderDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//订单号
	private String orderNumber;
	//付款人付款用户id
	private Integer userId;
	//支付状态
	private String status;
	//会员编号
	private String code;
	//来源
	private String source;
	//付款人姓名
	private String fromUsername;
	//收款人姓名
	private String toUsername;
	//创建时间
	private Date creatTime;
	//付款人账号
	private String fromAccount;
	//收款人账号
	private String toAccount;
	//付款金额
	private Double money;
	//支付方式
	private String payType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNumber() {
		return orderNumber;
	}
	/**
	 * 设置：付款人
付款用户id
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * 获取：付款人
付款用户id
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * 设置：支付状态
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：支付状态
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：来源
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * 获取：来源
	 */
	public String getSource() {
		return source;
	}
	/**
	 * 设置：付款人姓名
	 */
	public void setFromUsername(String fromUsername) {
		this.fromUsername = fromUsername;
	}
	/**
	 * 获取：付款人姓名
	 */
	public String getFromUsername() {
		return fromUsername;
	}
	/**
	 * 设置：收款人姓名
	 */
	public void setToUsername(String toUsername) {
		this.toUsername = toUsername;
	}
	/**
	 * 获取：收款人姓名
	 */
	public String getToUsername() {
		return toUsername;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatTime() {
		return creatTime;
	}
	/**
	 * 设置：付款人账号
	 */
	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}
	/**
	 * 获取：付款人账号
	 */
	public String getFromAccount() {
		return fromAccount;
	}
	/**
	 * 设置：收款人账号
	 */
	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}
	/**
	 * 获取：收款人账号
	 */
	public String getToAccount() {
		return toAccount;
	}
	/**
	 * 设置：付款金额
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * 获取：付款金额
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * 设置：支付方式
	 */
	public void setPayType(String payType) {
		this.payType = payType;
	}
	/**
	 * 获取：支付方式
	 */
	public String getPayType() {
		return payType;
	}
}
