package com.pojoin.system.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: aqie
 * @create: 2020-10-11 11:33
 **/
@RestController
@RequestMapping("test")
public class TestController {
    @GetMapping("hello")
    @ResponseBody
    public String hello(){
        return "hello hui";
    }
}
