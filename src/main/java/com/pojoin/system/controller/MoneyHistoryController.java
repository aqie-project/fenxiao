package com.pojoin.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pojoin.system.domain.MoneyHistoryDO;
import com.pojoin.system.service.MoneyHistoryService;
import com.pojoin.common.utils.PageUtils;
import com.pojoin.common.utils.Query;
import com.pojoin.common.utils.R;

/**
 * 资金流水记录
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-27 15:56:44
 */
 
@Controller
@RequestMapping("/system/moneyHistory")
public class MoneyHistoryController {
	@Autowired
	private MoneyHistoryService moneyHistoryService;
	
	@GetMapping("/{id}")
	String MoneyHistory(@PathVariable("id") Integer id, Model model){
		model.addAttribute("userId",id);
	    return "system/moneyHistory/moneyHistory";
	}
	
	@ResponseBody
	@GetMapping("/list")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<MoneyHistoryDO> moneyHistoryList = moneyHistoryService.list(query);
		int total = moneyHistoryService.count(query);
		PageUtils pageUtils = new PageUtils(moneyHistoryList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:moneyHistory:add")
	String add(){
	    return "system/moneyHistory/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:moneyHistory:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		MoneyHistoryDO moneyHistory = moneyHistoryService.get(id);
		model.addAttribute("moneyHistory", moneyHistory);
	    return "system/moneyHistory/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:moneyHistory:add")
	public R save( MoneyHistoryDO moneyHistory){
		if(moneyHistoryService.save(moneyHistory)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:moneyHistory:edit")
	public R update( MoneyHistoryDO moneyHistory){
		moneyHistoryService.update(moneyHistory);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:moneyHistory:remove")
	public R remove( Integer id){
		if(moneyHistoryService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:moneyHistory:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		moneyHistoryService.batchRemove(ids);
		return R.ok();
	}
	
}
