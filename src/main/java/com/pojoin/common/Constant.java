package com.pojoin.common;

/**
 * @author aqie
 * @date 2020-10-14 8:25
 * @function
 */
public class Constant {
    // 合伙人
    public static int PARTNER = 1;

    public static int GRADE_1 = 2;
    public static int GRADE_2 = 3;
    public static int GRADE_3 = 4;
    public static int GRADE_4 = 5;
    public static int GRADE_5 = 6;
    public static int GRADE_6 = 7;
    public static int GRADE_7 = 8;
    public static int GRADE_8 = 9;
    public static int GRADE_9 = 10;

}
