package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 资金流水记录
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-27 16:44:47
 */
public class MoneyHistoryDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//主键
	private Integer id;
	//用户id
	private Long userId;
	//现金
	private Double money;
	//积分
	private Double balance;
	//m:钱；b：积分
	private String type;
	//创建时间
	private Date creatTime;
	//
	private String bank1;
	//
	private String bank2;
	//来源
	private Long fromUserId;
	//备注
	private String remark;

	/**
	 * 设置：主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：现金
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * 获取：现金
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * 设置：积分
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	/**
	 * 获取：积分
	 */
	public Double getBalance() {
		return balance;
	}
	/**
	 * 设置：m:钱；b：积分
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * 获取：m:钱；b：积分
	 */
	public String getType() {
		return type;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatTime() {
		return creatTime;
	}
	/**
	 * 设置：
	 */
	public void setBank1(String bank1) {
		this.bank1 = bank1;
	}
	/**
	 * 获取：
	 */
	public String getBank1() {
		return bank1;
	}
	/**
	 * 设置：
	 */
	public void setBank2(String bank2) {
		this.bank2 = bank2;
	}
	/**
	 * 获取：
	 */
	public String getBank2() {
		return bank2;
	}
	/**
	 * 设置：来源
	 */
	public void setFromUserId(Long fromUserId) {
		this.fromUserId = fromUserId;
	}
	/**
	 * 获取：来源
	 */
	public Long getFromUserId() {
		return fromUserId;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
