$().ready(function() {

	validateRule();
    $("#yinhang1").click(function () {
        $("#yinhang").show();
        $("#zfb").hide();
        $("#aliPayAccount").val("");
    })
    $("#zfb1").click(function () {
        $("#yinhang").hide();
        $("#zfb").show();
        $("#accout").val("");
        $("#bank").val("");
        $("#address").val("");
    })
    $("#yinhang1").click();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/system/tixian/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				// parent.reLoad();
				// var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				// parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}





function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
            bank : {
				required : true
			},
            address:{
                required:true
			},
            accout: {
                required: true
            },
            phone: {
                required: true
            },
            money: {
                required: true
            },
            aliPayAccount: {
                required: true
            },
            bank2:{
                required: true
            }

		},
		messages : {
            aliPayAccount: {
                required: icon + "请输入支付宝账号"
            },
            bank2: {
                required: icon + "请输入真实姓名"
            },
		    bank : {
				required : icon + "请输入银行名称"
			},
            address: {
                required: icon + "请输入开户行详细地址"
            },
            accout: {
                required: icon + "请输入银行账号"
            },
            phone: {
                required: icon + "请输入提款人电话"
            },
            money: {
                required: icon + "请输入金额"
            }
		}
	})
}