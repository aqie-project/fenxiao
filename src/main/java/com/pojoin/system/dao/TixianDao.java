package com.pojoin.system.dao;

import com.pojoin.system.domain.TixianDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 提现表
 * @author pojoin
 * @email 
 * @date 2020-06-30 19:43:49
 */
@Mapper
public interface TixianDao {

	TixianDO get(Integer id);
	
	List<TixianDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(TixianDO tixian);
	
	int update(TixianDO tixian);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
