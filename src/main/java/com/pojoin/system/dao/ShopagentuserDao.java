package com.pojoin.system.dao;

import com.pojoin.system.domain.ShopagentuserDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 代理商
 * @author pojoin
 * @email 
 * @date 2020-07-15 17:59:34
 */
@Mapper
public interface ShopagentuserDao {

	ShopagentuserDO get(Integer id);
	
	List<ShopagentuserDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ShopagentuserDO shopagentuser);
	
	int update(ShopagentuserDO shopagentuser);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
