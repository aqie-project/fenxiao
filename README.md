### 10-25 执行sql
-
```
ALTER TABLE `sys_user` 
ADD COLUMN `num` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '邀请人数' AFTER `partner`;
UPDATE `sys_user` SET `grade` = '九星' WHERE (`user_id` = '1');

```

```
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE `hhlm`.`sys_user` 
ADD COLUMN `partner` int NULL COMMENT '0: 普通 1：合伙人 2：董事 ' AFTER `ali_pay_name`;
update sys_user set partner=1 where grade = '合伙人';
SELECT * FROM hhlm.sys_user where grade='合伙人';
-- 添加合伙人菜单
INSERT INTO `hhlm`.`sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`) VALUES ('136', '113', '合伙人列表', '/sys/mem/partner1', '', '1', 'fa fa-address-book-o');
INSERT INTO `hhlm`.`sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('4179', '1', '136');
INSERT INTO `hhlm`.`sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('4180', '61', '136');
ALTER TABLE `hhlm`.`sys_user` 
ADD COLUMN `option` double(10, 2) NULL DEFAULT 0 COMMENT '期权' AFTER `ali_pay_name`;


SELECT * FROM hhlm.sys_user where mobile=13207145534;
SELECT * FROM hhlm.sys_user where user_id in (0,149,25367,27285,28802,38366,38387)

SELECT name,user_id,username,count(*) FROM hhlm.sys_user group by invitate_code

SET SQL_SAFE_UPDATES = 0;
update sys_user set partner=0 where partner=1;
```


#### 插入数据
```
com.pojoin.system.service.impl.UserServiceImpl.insertUsers
SELECT count(*) FROM hhlm.sys_user;  7 10 17(2) 57(3) 107(4) 307(5) 607(6) 1007(7) 2007(8) 
5170 
SET SQL_SAFE_UPDATES = 0;
delete from sys_user where user_id >44041;
```


- [批量插入](https://blog.csdn.net/wlwlwlwl015/article/details/50246717)
https://blog.csdn.net/xuyudian/article/details/88576063?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.channel_param

nohup java -jar market-2.0.0.jar &

#### 待办任务
- 数据库更新合伙人,新增等级
- 重写等级返利逻辑
- 会员列表编辑添加等级
- 修改会员列表升级六星会触发合伙人逻辑
- 去掉董事
- 合伙人直推怎么算等级差 *
- 升级用户等级
    + 解决之前逻辑数据插入 升级九星卡死
- 合伙人
    + 修改成为合伙人逻辑
    + 用户升星，父级邀请人判断是否升级
        * com.pojoin.system.service.impl.UserServiceImpl.updateUserLevel
    + 注册新用户添加合伙人 (有两个五星升级为合伙人), 移除董事
        * 
    + 分钱计算合伙人，移除董事
    + 后台添加合伙人列表 及页面   
- 升级合伙人 会提升星级 
- 手动升级父级自动升级    
    
- com.pojoin.system.dao.GradeDao.get
    + 这里大改    
- 查询程序所有和 sys_grade 写死相关逻辑    
- 星级修改比例    

#### 测试
- 推荐人 15369929836
- 报单中心 18131823456

0.1 0.12 0.14 0.16 0.18 0.20 0.22 0.24 0.26
注册人 44036
0,149,25367,27285,28802,38366,38387
38387-贾淑锐15369929836(二星-0.12) -> 38366-李红霞13315868505(四星-0.16) -> 28802-王15390415509(五星-0.18) ->27285(六星0.20) ->25367(九星0.26)
 43.8(0.12)                                    14.6(0.04)                              7.3(0.02)             7.3(0.02)     21.9(0.06)
 
 53邀请人
 53(0) -> 43(8-0.24)     -11(9-0,26)
            87.6(0.24)    7.3(0.02)
            
52-0 51-0 21-5-0.18 22-9-0.26 
51 -> 6
52 邀请人   
52-0 51-6-0.2 21-5-0.18 22-9-0.26  
      73                29.2
 
 
 
54-87330 -> 53-85330
541 -> 53
 
#### ceshi
-  推荐人 13 邀请 14 22
- 14 邀请 15 23
- 15 邀请 16 24
- 62   74 75
- 报单中心 12345

#### problem
- 邀请人挂在推荐人下边，现在挂在了报单人下面
- 55是二级代理，是报单中心
- 12 邀请 21 13，没有升级为合伙人

####  com.mysql.cj.jdbc.exceptions.MySQLTransactionRollbackException: Lock wait timeout exceeded; try restarting transaction
- select * from information_schema.innodb_trx


#### 待办任务
- 注册一个一级代理是四星
- 推荐一个省代理直接就是五星
- 手动升级，父级添加相应人数，父级添加人数为子集添加人数
    + 手动提高星级之后，分润的计算，或多分出去一节 ： 子代比我星级还高
    + 父级不是五星邀请两个五星 不会成为合伙人

```
统计用户4 邀请人数
SELECT username,money,invitate_code,num FROM hhlm.sys_user where code=4 or invitate_code=4;


``` 
- 增加邀请人数   
 com.pojoin.system.service.impl.UserServiceImpl.getGrade
 
```
重置数据
SET SQL_SAFE_UPDATES = 0;
update hhlm.sys_user set `grade`="无",num=0  where username='a2';
update hhlm.sys_user set `grade`="无",num=1  where username='a1';
delete from hhlm.sys_user where invitate_code in ('a1','a2') and username <> 'a2';

查看更新结果
SELECT user_id, username,grade,partner,money,invitate_code,num,parent_link FROM hhlm.sys_user where user_id in(149,25367,87982,94399,94400) or invitate_code in('a2','a1');

``` 
- 测试手动升星
    + a1 -> a2 （149,25367,87982,94399,94400）
        + a1 有 1 个人 a2 0人
        + a2 手动升一星
            * todo
                + 增加 a2 num
                + 增加 a1 num
                + a1 升 1星
                + a2 的所有 父级增加 num
- 测试升级合伙人
    + a1 邀请 a2 a3
        + a2 a3 手动升级五星  
- a1 邀请一个人 增加 num  

#### 测试分红   
91-无 16-五星(0.18) 15-六星(0.20) 14-七星(0.22)  13-八星(0.24)  12-九星(0.26)   11-九星   
        65.7        7.3           7.3           7.3               7.3

                                  14.6                                        32.85
```
 user_id in(149,25367,87982,94399,94400) or invitate_code in('a2','a1') or 
SELECT user_id, username,grade,partner,money,invitate_code,num,parent_link FROM hhlm.sys_user where username=92;
149,25367,87982,87992-12,87993-13,87994-14,87995-15,88992-16 ,97353-92,98547-94
```                                  
93   92-无   16-六星(0.2) 15-七星(0.22) 14-八星(0.24)  13-八星(0.24)  12-九星(0.26)   11-九星   
                  65.7        7.3           7.3           7.3               7.3
              
                  73-0.2         7.3           7.3           0                 40.15
21-五星-89592-0.18 12-九星-87992-0.26
21 邀请 95                 