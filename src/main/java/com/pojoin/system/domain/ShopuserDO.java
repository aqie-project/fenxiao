package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 用户表
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-15 17:59:35
 */
public class ShopuserDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//头像
	private String iconimage;
	//昵称
	private String name;
	//用户编号
	private Long code;
	//现金
	private Double cash;
	//服务费
	private Double cash1;
	//优惠券
	private Double cash2;
	//K币
	private Double cash3;
	//是否关联门店
	private Integer ishasbusness;
	//月收益
	private Double profit;
	//本月订单
	private Integer order;
	//未处理订单
	private Integer order1;
	//是否认证
	private String very;
	//电话
	private String phone;
	//性别
	private String sex;
	//出生日期
	private String birthday;
	//所在地区
	private String address;
	//个人简介
	private String intro;
	//从事行业
	private String industry;
	//所属单位
	private String company;
	//职位名称
	private String duty;
	//
	private String password;
	//
	private String verymsg;
	//
	private String identityid;
	//
	private String invitecode;
	//
	private String logintoken;
	//公众号OpenId
	private String pubopenid;
	//
	private String alipayuid;
	//微信APP授权OpenId
	private String openid;
	//日收益
	private Double daycash;
	//
	private String os;
	//
	private String deviceid;
	//是否是代理商；1是代理商；
	private Integer isagent;
	//
	private String agenttype;
	//
	private Integer point;
	//
	private Date createtime;
	//支付宝账号信息
	private String alipayaccount;
	//
	private String alipayname;
	//云喇叭ID
	private String hornid;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：头像
	 */
	public void setIconimage(String iconimage) {
		this.iconimage = iconimage;
	}
	/**
	 * 获取：头像
	 */
	public String getIconimage() {
		return iconimage;
	}
	/**
	 * 设置：昵称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：昵称
	 */
	public String getName() {
		return name;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	/**
	 * 设置：现金
	 */
	public void setCash(Double cash) {
		this.cash = cash;
	}
	/**
	 * 获取：现金
	 */
	public Double getCash() {
		return cash;
	}
	/**
	 * 设置：服务费
	 */
	public void setCash1(Double cash1) {
		this.cash1 = cash1;
	}
	/**
	 * 获取：服务费
	 */
	public Double getCash1() {
		return cash1;
	}
	/**
	 * 设置：优惠券
	 */
	public void setCash2(Double cash2) {
		this.cash2 = cash2;
	}
	/**
	 * 获取：优惠券
	 */
	public Double getCash2() {
		return cash2;
	}
	/**
	 * 设置：K币
	 */
	public void setCash3(Double cash3) {
		this.cash3 = cash3;
	}
	/**
	 * 获取：K币
	 */
	public Double getCash3() {
		return cash3;
	}
	/**
	 * 设置：是否关联门店
	 */
	public void setIshasbusness(Integer ishasbusness) {
		this.ishasbusness = ishasbusness;
	}
	/**
	 * 获取：是否关联门店
	 */
	public Integer getIshasbusness() {
		return ishasbusness;
	}
	/**
	 * 设置：月收益
	 */
	public void setProfit(Double profit) {
		this.profit = profit;
	}
	/**
	 * 获取：月收益
	 */
	public Double getProfit() {
		return profit;
	}
	/**
	 * 设置：本月订单
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}
	/**
	 * 获取：本月订单
	 */
	public Integer getOrder() {
		return order;
	}
	/**
	 * 设置：未处理订单
	 */
	public void setOrder1(Integer order1) {
		this.order1 = order1;
	}
	/**
	 * 获取：未处理订单
	 */
	public Integer getOrder1() {
		return order1;
	}
	/**
	 * 设置：是否认证
	 */
	public void setVery(String very) {
		this.very = very;
	}
	/**
	 * 获取：是否认证
	 */
	public String getVery() {
		return very;
	}
	/**
	 * 设置：电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：性别
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * 设置：出生日期
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	/**
	 * 获取：出生日期
	 */
	public String getBirthday() {
		return birthday;
	}
	/**
	 * 设置：所在地区
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：所在地区
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：个人简介
	 */
	public void setIntro(String intro) {
		this.intro = intro;
	}
	/**
	 * 获取：个人简介
	 */
	public String getIntro() {
		return intro;
	}
	/**
	 * 设置：从事行业
	 */
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	/**
	 * 获取：从事行业
	 */
	public String getIndustry() {
		return industry;
	}
	/**
	 * 设置：所属单位
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * 获取：所属单位
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * 设置：职位名称
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	/**
	 * 获取：职位名称
	 */
	public String getDuty() {
		return duty;
	}
	/**
	 * 设置：
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * 获取：
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * 设置：
	 */
	public void setVerymsg(String verymsg) {
		this.verymsg = verymsg;
	}
	/**
	 * 获取：
	 */
	public String getVerymsg() {
		return verymsg;
	}
	/**
	 * 设置：
	 */
	public void setIdentityid(String identityid) {
		this.identityid = identityid;
	}
	/**
	 * 获取：
	 */
	public String getIdentityid() {
		return identityid;
	}
	/**
	 * 设置：
	 */
	public void setInvitecode(String invitecode) {
		this.invitecode = invitecode;
	}
	/**
	 * 获取：
	 */
	public String getInvitecode() {
		return invitecode;
	}
	/**
	 * 设置：
	 */
	public void setLogintoken(String logintoken) {
		this.logintoken = logintoken;
	}
	/**
	 * 获取：
	 */
	public String getLogintoken() {
		return logintoken;
	}
	/**
	 * 设置：公众号OpenId
	 */
	public void setPubopenid(String pubopenid) {
		this.pubopenid = pubopenid;
	}
	/**
	 * 获取：公众号OpenId
	 */
	public String getPubopenid() {
		return pubopenid;
	}
	/**
	 * 设置：
	 */
	public void setAlipayuid(String alipayuid) {
		this.alipayuid = alipayuid;
	}
	/**
	 * 获取：
	 */
	public String getAlipayuid() {
		return alipayuid;
	}
	/**
	 * 设置：微信APP授权OpenId
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * 获取：微信APP授权OpenId
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * 设置：日收益
	 */
	public void setDaycash(Double daycash) {
		this.daycash = daycash;
	}
	/**
	 * 获取：日收益
	 */
	public Double getDaycash() {
		return daycash;
	}
	/**
	 * 设置：
	 */
	public void setOs(String os) {
		this.os = os;
	}
	/**
	 * 获取：
	 */
	public String getOs() {
		return os;
	}
	/**
	 * 设置：
	 */
	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}
	/**
	 * 获取：
	 */
	public String getDeviceid() {
		return deviceid;
	}
	/**
	 * 设置：是否是代理商；1是代理商；
	 */
	public void setIsagent(Integer isagent) {
		this.isagent = isagent;
	}
	/**
	 * 获取：是否是代理商；1是代理商；
	 */
	public Integer getIsagent() {
		return isagent;
	}
	/**
	 * 设置：
	 */
	public void setAgenttype(String agenttype) {
		this.agenttype = agenttype;
	}
	/**
	 * 获取：
	 */
	public String getAgenttype() {
		return agenttype;
	}
	/**
	 * 设置：
	 */
	public void setPoint(Integer point) {
		this.point = point;
	}
	/**
	 * 获取：
	 */
	public Integer getPoint() {
		return point;
	}
	/**
	 * 设置：
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：支付宝账号信息
	 */
	public void setAlipayaccount(String alipayaccount) {
		this.alipayaccount = alipayaccount;
	}
	/**
	 * 获取：支付宝账号信息
	 */
	public String getAlipayaccount() {
		return alipayaccount;
	}
	/**
	 * 设置：
	 */
	public void setAlipayname(String alipayname) {
		this.alipayname = alipayname;
	}
	/**
	 * 获取：
	 */
	public String getAlipayname() {
		return alipayname;
	}
	/**
	 * 设置：云喇叭ID
	 */
	public void setHornid(String hornid) {
		this.hornid = hornid;
	}
	/**
	 * 获取：云喇叭ID
	 */
	public String getHornid() {
		return hornid;
	}
}
