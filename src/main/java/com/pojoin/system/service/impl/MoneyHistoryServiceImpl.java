package com.pojoin.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.pojoin.system.dao.MoneyHistoryDao;
import com.pojoin.system.domain.MoneyHistoryDO;
import com.pojoin.system.service.MoneyHistoryService;



@Service
public class MoneyHistoryServiceImpl implements MoneyHistoryService {
	@Autowired
	private MoneyHistoryDao moneyHistoryDao;
	
	@Override
	public MoneyHistoryDO get(Integer id){
		return moneyHistoryDao.get(id);
	}
	
	@Override
	public List<MoneyHistoryDO> list(Map<String, Object> map){
		return moneyHistoryDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return moneyHistoryDao.count(map);
	}
	
	@Override
	public int save(MoneyHistoryDO moneyHistory){
		return moneyHistoryDao.save(moneyHistory);
	}
	
	@Override
	public int update(MoneyHistoryDO moneyHistory){
		return moneyHistoryDao.update(moneyHistory);
	}
	
	@Override
	public int remove(Integer id){
		return moneyHistoryDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return moneyHistoryDao.batchRemove(ids);
	}

	@Override
	public int addMoney(Long userId, Double money, Double balanc, Long fromUserId, String remark, String type,String moneyHistry, String balanbeHistory) {
		MoneyHistoryDO moneyHistoryDO=new MoneyHistoryDO();
		moneyHistoryDO.setUserId(userId);
		moneyHistoryDO.setFromUserId(fromUserId);
		moneyHistoryDO.setBalance(balanc);
		moneyHistoryDO.setMoney(money);
		moneyHistoryDO.setRemark(remark);
		moneyHistoryDO.setCreatTime(new Date());
		moneyHistoryDO.setType(type);
		moneyHistoryDO.setBank1(moneyHistry);
		moneyHistoryDO.setBank2(balanbeHistory);
		return moneyHistoryDao.save(moneyHistoryDO);
	}


}
