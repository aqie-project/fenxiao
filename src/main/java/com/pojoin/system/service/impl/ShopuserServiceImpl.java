package com.pojoin.system.service.impl;

import com.pojoin.system.dao.*;
import com.pojoin.system.domain.*;
import com.pojoin.system.feign.PayFeign;
import com.pojoin.system.feign.ShopFeign;
import com.pojoin.system.service.ShopuserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Service
public class ShopuserServiceImpl implements ShopuserService {
    @Autowired
    private ShopuserDao shopuserDao;

    @Autowired
    private UserDao userDao;
    @Autowired
    private ShopDao shopDao;
    @Autowired
    private ShopmoneyDao shopmoneyDao;
    @Autowired
    private ShopchanneluserDao shopchanneluserDao;
    @Autowired
    private ShopagentuserDao shopagentuserDao;

    @Override
    public ShopuserDO get(Integer id) {
        return shopuserDao.get(id);
    }

    @Override
    public List<ShopuserDO> list(Map<String, Object> map) {
        return shopuserDao.list(map);
    }

    @Override
    public ShopuserDO getByPhone(String phone) {
        return shopuserDao.getByPhone(phone);
    }

    @Override
    public int count(Map<String, Object> map) {
        return shopuserDao.count(map);
    }

    @Override
    public int save(ShopuserDO shopuser) {
        return shopuserDao.save(shopuser);
    }

    @Override
    public int update(ShopuserDO shopuser) {
        return shopuserDao.update(shopuser);
    }

    @Override
    public int remove(Integer id) {
        return shopuserDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return shopuserDao.batchRemove(ids);
    }
    @Autowired
    ShopuserService shopuserService;

    @Autowired
    PayFeign payFeign;
    @Autowired
    ShopFeign shopFeign;


    @Override
    public int save(UserDO newUser) {
        ShopuserDO shopuserDO = shopuserService.getByPhone(newUser.getCode());
        ShopuserDO appShopUser = new ShopuserDO();
        //获取报单中心
        UserDO coreUser = userDao.getByCore(newUser.getCoreCode());
        //查询报单中心对应的app用户
        ShopuserDO coreShopUser = shopuserDao.getByPhone(coreUser.getMobile());
        if(coreShopUser==null){
            return 2;
        }
        //app邀请码
        Long inviteCode = coreShopUser.getCode();
        appShopUser.setName(newUser.getName());
        appShopUser.setPassword("111111");
        appShopUser.setPhone(newUser.getMobile());
        appShopUser.setInvitecode(inviteCode.toString());
        appShopUser.setHornid("");
        appShopUser.setLogintoken(UUID.randomUUID().toString());
        appShopUser.setPoint(0);
        appShopUser.setCreatetime(new Date());
        appShopUser.setAddress(newUser.getDistrict());
        appShopUser.setCode(Long.valueOf(newUser.getMobile()));


        appShopUser.setIconimage("");
        appShopUser.setCash(0d);
        appShopUser.setCash1(0d);
        appShopUser.setCash2(0d);
        appShopUser.setCash3(0d);
        appShopUser.setProfit(0d);
        appShopUser.setVery("1");
        appShopUser.setDaycash(0d);
        appShopUser.setAlipayaccount("");
        appShopUser.setAlipayuid("");

        if ("0".equals(newUser.getLevel())) {
            //商店
            if (shopuserDO == null) {
                appShopUser.setIshasbusness(1);
                shopuserDao.save(appShopUser);
//                ShopmoneyDO shopmoneyDO = new ShopmoneyDO();
//                shopmoneyDO.setCoupon(200d);
//                shopmoneyDO.setCharge(0d);
//                shopmoneyDO.setMoney(0d);
//                shopmoneyDO.setCreatetime(new Date());
//                shopmoneyDO.setUserid(appShopUser.getId());
//                shopmoneyDao.save(shopmoneyDO);
                //payFeign.addCoupon(appShopUser.getId(),200d,0);
                ShopDO newshop = new ShopDO();
                newshop.setInvitecode(inviteCode.toString());
                newshop.setUserid(appShopUser.getId());
                newshop.setCreatetime(new Date());
                newshop.setAddress(newUser.getDistrict());
                newshop.setMaxdiscount(10.0);
                newshop.setDiscount("9.9");
                newshop.setCellphone(appShopUser.getPhone());
                newshop.setName("");
                newshop.setCity("");
                newshop.setXpoint("0");
                newshop.setYpoint("0");
                newshop.setCategory("");
                newshop.setUsername(appShopUser.getName());
                newshop.setDesc("");
                newshop.setShoptype("");
                newshop.setIshasbusness(1);
                newshop.setEvaluatescore("5");
                newshop.setLogo("");
                newshop.setIcon("");
                //shopDao.save(newshop);
                //shopFeign.addShop(newshop);
            }else{
//                ShopuserDO appShopUser1 = new ShopuserDO();
//                appShopUser1.setId(shopuserDO.getId());
//                appShopUser1.setCode(Long.valueOf(newUser.getMobile()));
//                shopuserDao.update(appShopUser1);
            }

        } else if ("1".equals(newUser.getLevel())) {
            //社区、二级代理=渠道商
            if (shopuserDO == null) {
                appShopUser.setIsagent(0);
                shopuserDao.save(appShopUser);
//                ShopmoneyDO shopmoneyDO = new ShopmoneyDO();
//                shopmoneyDO.setCoupon(200d);
//                shopmoneyDO.setCharge(0d);
//                shopmoneyDO.setMoney(0d);
//                shopmoneyDO.setCreatetime(new Date());
//                shopmoneyDO.setUserid(appShopUser.getId());
//                shopmoneyDao.save(shopmoneyDO);
                //payFeign.addCoupon(appShopUser.getId(), 200d, 0);
                ShopchanneluserDO shopchanneluserDO = new ShopchanneluserDO();
                shopchanneluserDO.setPhone(newUser.getMobile());
                shopchanneluserDO.setUserid(appShopUser.getId());
                shopchanneluserDO.setCreatetime(new Date());
                shopchanneluserDO.setUsername(appShopUser.getName());
                shopchanneluserDO.setAddress(appShopUser.getAddress());
                shopchanneluserDO.setIdentcard(appShopUser.getIdentityid());
                shopchanneluserDO.setInvitecode(inviteCode.toString());
                shopchanneluserDao.save(shopchanneluserDO);
            }else{

            }
        } else if ("2".equals(newUser.getLevel())) {
            //区县、一级代理= 区代理商  区/县代理商
            if (shopuserDO == null) {
                appShopUser.setIsagent(1);
                appShopUser.setAgenttype("区代理商");
                shopuserDao.save(appShopUser);
//                ShopmoneyDO shopmoneyDO = new ShopmoneyDO();
//                shopmoneyDO.setCoupon(200d);
//                shopmoneyDO.setCharge(0d);
//                shopmoneyDO.setMoney(0d);
//                shopmoneyDO.setCreatetime(new Date());
//                shopmoneyDO.setUserid(appShopUser.getId());
//                shopmoneyDao.save(shopmoneyDO);
                //payFeign.addCoupon(appShopUser.getId(), 200d, 0);
                ShopagentuserDO shopagentuserDO = new ShopagentuserDO();
                shopagentuserDO.setAgenttype("区/县代理商");
                shopagentuserDO.setUserid(appShopUser.getId());
                shopagentuserDO.setPhone(newUser.getMobile());
                shopagentuserDO.setCreatetime(new Date());
                shopagentuserDO.setArea(appShopUser.getAddress());
                shopagentuserDO.setInvitecode(inviteCode.toString());
                shopagentuserDao.save(shopagentuserDO);
            }else{

            }

        } else if ("3".equals(newUser.getLevel())) {
            //市代=市代理商
            if (shopuserDO == null) {
                appShopUser.setIsagent(1);
                appShopUser.setAgenttype("市代理商");
                shopuserDao.save(appShopUser);
//                ShopmoneyDO shopmoneyDO = new ShopmoneyDO();
//                shopmoneyDO.setCoupon(200d);
//                shopmoneyDO.setCharge(0d);
//                shopmoneyDO.setMoney(0d);
//                shopmoneyDO.setCreatetime(new Date());
//                shopmoneyDO.setUserid(appShopUser.getId());
//                shopmoneyDao.save(shopmoneyDO);
                payFeign.addCoupon(appShopUser.getId(), 200d, 0);
                ShopagentuserDO shopagentuserDO = new ShopagentuserDO();
                shopagentuserDO.setAgenttype("市代理商");
                shopagentuserDO.setUserid(appShopUser.getId());
                shopagentuserDO.setPhone(newUser.getMobile());
                shopagentuserDO.setCreatetime(new Date());
                shopagentuserDO.setArea(appShopUser.getAddress());
                shopagentuserDO.setInvitecode(inviteCode.toString());
                shopagentuserDao.save(shopagentuserDO);
            }else{


            }


        }


        return 0;
    }


}
