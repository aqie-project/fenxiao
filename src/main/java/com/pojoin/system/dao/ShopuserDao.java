package com.pojoin.system.dao;

import com.pojoin.system.domain.ShopuserDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表
 * @author pojoin
 * @email 
 * @date 2020-07-15 17:59:35
 */
@Mapper
public interface ShopuserDao {

	ShopuserDO get(Integer id);
	ShopuserDO getByPhone(String phone);
	List<ShopuserDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ShopuserDO shopuser);
	
	int update(ShopuserDO shopuser);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
