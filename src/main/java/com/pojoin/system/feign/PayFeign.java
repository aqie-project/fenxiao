package com.pojoin.system.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @version 2.0.0
 * @auther liyangzhu
 * @since 2.0.0 2020/9/4
 */
@FeignClient(name = "PAY", path = "/pay")
public interface PayFeign {
    @RequestMapping("/money/addCoupon")
    void addCoupon(@RequestParam(name = "userId") int userId,
                         @RequestParam(name = "coupon") double coupon,
                         @RequestParam(name = "shopId") int shopId);
}
