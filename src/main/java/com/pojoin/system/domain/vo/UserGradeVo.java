package com.pojoin.system.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户星级及其百分比
 * @author: aqie
 * @create: 2020-10-11 18:20
 **/
@Data
public class UserGradeVo implements Serializable {
    private Long userId;

    private String grade;

    private Double proportion;

}
