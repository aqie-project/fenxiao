package com.pojoin.system.service;

import com.pojoin.system.domain.CurrencyHistoryDO;

import java.util.List;
import java.util.Map;

/**
 * @version 2.0.0
 * @auther liyangzhu
 * @since 2.0.0 2020/6/29
 */
public interface CurrencyHistoryService {

    List<Map<String, Object>> list(Map<String, Object> map);

    int count(Map<String, Object> map);
}
