package com.pojoin.system.service.impl;

import com.pojoin.system.dao.CurrencyHistoryDao;
import com.pojoin.system.domain.CurrencyHistoryDO;
import com.pojoin.system.service.CurrencyHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @version 2.0.0
 * @auther liyangzhu
 * @since 2.0.0 2020/6/29
 */

@Service
public class CurrencyHistoryServiceImpl implements CurrencyHistoryService {

    @Autowired
    CurrencyHistoryDao currencyHistoryDao;


    @Override
    public List<Map<String, Object>> list(Map<String, Object> map) {
        return currencyHistoryDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return currencyHistoryDao.count(map);
    }
}
