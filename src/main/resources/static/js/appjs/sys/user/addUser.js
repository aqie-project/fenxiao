$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function getCheckedRoles() {
	var adIds = "";
	$("input:checkbox[name=role]:checked").each(function(i) {
		if (0 == i) {
			adIds = $(this).val();
		} else {
			adIds += ("," + $(this).val());
		}
	});
	return adIds;
}
function save() {
if($("#level").val()=="-1"){
    layer.alert("请选择会员级别！");
	return;
}

	//$("#roleIds").val(getCheckedRoles());
	$.ajax({
		cache : true,
		type : "POST",
		url : "/sys/mem/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				//var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				//parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
			name : {
				required : true
			},
			username : {
				required : true,
				minlength : 2,
				remote : {
					url : "/sys/mem/save", // 后台处理程序
					type : "post", // 数据发送方式
					dataType : "json", // 接受数据格式
					data : { // 要传递的数据
						username : function() {
							return $("#username").val();
						}
					}
				}
			},
			password : {
				required : true,
				minlength : 6
			},
			confirm_password : {
				required : true,
				minlength : 6,
				equalTo : "#password"
			},
            // idcard:{
            //     required: true
			// 	},
            mobile:{
                required:true,
                maxlength: 11
			},
            invitateCode:{
                required: true,
                maxlength: 11
			},
            coreCode:{
                required: true,
                maxlength: 11
			}
			//,
			// district: {
            //     required: true,
            //     maxlength: 11
            // },
			// topic : {
			// 	required : "#newsletter:checked",
			// 	minlength : 2
			// },
			// agree : "required"
		},
		messages : {

			name : {
				required : icon + "请输入姓名"
			},
			username : {
				required : icon + "请输入您的用户名",
				minlength : icon + "用户名必须两个字符以上",
				remote : icon + "用户名已经存在"
			},
			password : {
				required : icon + "请输入您的密码",
				minlength : icon + "密码必须6个字符以上"
			}
            ,
            // idcard: {
            //     required: icon + "请输入身份证号",
            // },
            mobile: {
                required: icon + "请输入手机号",
            },
			confirm_password : {
				required : icon + "请再次输入密码",
				minlength : icon + "密码必须6个字符以上",
				equalTo : icon + "两次输入的密码不一致"
			},
			//email : icon + "请输入您的E-mail",
		}
	})
}

var openDept = function(){
	layer.open({
		type:2,
		title:"选择部门",
		area : [ '300px', '450px' ],
		content:"/system/sysDept/treeView"
	})
}
function loadDept( deptId,deptName){
	$("#deptId").val(deptId);
	$("#deptName").val(deptName);
}

$(function () {
    $("#invitateCode").bind('input propertychange', function () {
        var _this = $("#invitateCode").val();
        $.ajax({
            type: "POST",
            url: "/sys/mem/getnameBycode",
            data: {invitateCode: _this},
            success: function (data) {
                console.log(JSON.stringify(data))
                if (data.code == 0) {
                    $("#tname").text(data.msg);
                    $("#showname1").show();
                }
            }
        });
    })
    $("#coreCode").bind('input propertychange', function () {
        var _this = $("#coreCode").val();
        $.ajax({
            type: "POST",
            url: "/sys/mem/getnameBycode",
            data: {invitateCode: _this},
            success: function (data) {
                console.log(JSON.stringify(data))
                if (data.code == 0) {
                    $("#bname").text(data.msg);
                    $("#showname2").show();
                }
            }
        });
    })
})


//
// function getnameBycode() {
// 	var invitateCode = $("#invitateCode").val();
//     if(invitateCode==""){
//         parent.layer.alert("请输入邀请码");
//         return;
// 	}
// 	$.ajax({
//         cache : true,
//         type : "POST",
//         url : "/sys/mem/getnameBycode",
//         data : {invitateCode:invitateCode},
//         async : false,
//         success : function(data) {
//             parent.layer.alert(data.msg)
//         }
//     });
// }
// function getnameBycode1() {
//     var invitateCode = $("#coreCode").val();
//     if (invitateCode == "") {
//         parent.layer.alert("请输入报单中心");
//         return;
//     }
//     $.ajax({
//         cache : true,
//         type : "POST",
//         url : "/sys/mem/getnameBycode",
//         data : {invitateCode:invitateCode},
//         async : false,
//         success : function(data) {
//             parent.layer.alert(data.msg)
//         }
//     });
//}
