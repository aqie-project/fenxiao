package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 提现表
 * 
 * @author pojoin
 * @email 
 * @date 2020-06-30 19:43:49
 */
public class TixianDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//id主键
	private Integer id;
	//用户id
	private Integer userId;
	//开户行支行
	private String bank;
	//地址
	private String address;
	//开户行账号
	private String accout;
	//提款人电话
	private String phone;
	//提款金额
	private String money;
	//备注
	private String remark;
	//备用字段
	private String bank1;
	//备用字段
	private String bank2;
	//手续费
	private String serviceMoney;
	//余额
	private String balance;
	//状态0：未支付；1：已支付
	private String status;
	//创建时间
	private Date creatTime;
	//提款时间
	private Date payTime;
	//提现方式：微信：1；支付宝；2；银行卡3
	private String payType;

	private String aliPayAccount;
	private String aliPayName;


	/**
	 * 设置：id主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：id主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * 设置：开户行支行
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}
	/**
	 * 获取：开户行支行
	 */
	public String getBank() {
		return bank;
	}
	/**
	 * 设置：地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：开户行账号
	 */
	public void setAccout(String accout) {
		this.accout = accout;
	}
	/**
	 * 获取：开户行账号
	 */
	public String getAccout() {
		return accout;
	}
	/**
	 * 设置：提款人电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：提款人电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：提款金额
	 */
	public void setMoney(String money) {
		this.money = money;
	}
	/**
	 * 获取：提款金额
	 */
	public String getMoney() {
		return money;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：备用字段
	 */
	public void setBank1(String bank1) {
		this.bank1 = bank1;
	}
	/**
	 * 获取：备用字段
	 */
	public String getBank1() {
		return bank1;
	}
	/**
	 * 设置：备用字段
	 */
	public void setBank2(String bank2) {
		this.bank2 = bank2;
	}
	/**
	 * 获取：备用字段
	 */
	public String getBank2() {
		return bank2;
	}
	/**
	 * 设置：手续费
	 */
	public void setServiceMoney(String serviceMoney) {
		this.serviceMoney = serviceMoney;
	}
	/**
	 * 获取：手续费
	 */
	public String getServiceMoney() {
		return serviceMoney;
	}
	/**
	 * 设置：余额
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}
	/**
	 * 获取：余额
	 */
	public String getBalance() {
		return balance;
	}
	/**
	 * 设置：状态0：未支付；1：已支付
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态0：未支付；1：已支付
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatTime() {
		return creatTime;
	}
	/**
	 * 设置：提款时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：提款时间
	 */
	public Date getPayTime() {
		return payTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getAliPayAccount() {
		return aliPayAccount;
	}

	public void setAliPayAccount(String aliPayAccount) {
		this.aliPayAccount = aliPayAccount;
	}

	public String getAliPayName() {
		return aliPayName;
	}

	public void setAliPayName(String aliPayName) {
		this.aliPayName = aliPayName;
	}
}
