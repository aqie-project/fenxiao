package com.pojoin.system.controller;

import com.pojoin.common.annotation.Log;
import com.pojoin.common.controller.BaseController;
import com.pojoin.common.domain.Tree;
import com.pojoin.common.service.DictService;
import com.pojoin.common.utils.MD5Utils;
import com.pojoin.common.utils.PageUtils;
import com.pojoin.common.utils.Query;
import com.pojoin.common.utils.R;
import com.pojoin.system.dao.UserDao;
import com.pojoin.system.domain.DeptDO;
import com.pojoin.system.domain.OrderDO;
import com.pojoin.system.domain.RoleDO;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.service.*;
import com.pojoin.system.vo.UserVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/sys/fin")
@Controller
public class FinanceController extends BaseController {
	private String prefix="finance/list";
	@Autowired
	FinanceService financeService;
	@Autowired
	MoneyHistoryService moneyHistoryService;
	//收入明细
	@GetMapping("")
	String user(Model model) {
		model.addAttribute("userId",getUserId());
		return prefix + "/order";
	}

	@Log("充值")
	@GetMapping("/add")
	String add(Model model) {
		model.addAttribute("userId",getUserId());
		return prefix + "/add";
	}

	@Log("提现")
	@GetMapping("/tixian")
	String tixian(Model model) {
		return prefix + "/tixian";
	}


//	@Log("提现记录")
////	@GetMapping("/tixianlist")
////	String tixianlist(Model model) {
////		return prefix + "/tixianlist";
////	}

	@Log("充值记录")
	@GetMapping("/rechargelist")
	String rechargelist(Model model) {
		return prefix + "/rechargelist";
	}

	@Log("重积分页面")
	@GetMapping("/addintegral")
	String addintegral(Model model) {
		return prefix + "/addintegral";
	}

	@PostMapping("/addOrder")
	@Log("保存充值订单")
	@ResponseBody
	public R save(OrderDO orderDO){
		orderDO.setCreatTime(new Date());
		//0已付款未确定；1已付款已确定
		orderDO.setStatus("0");
		String outTradeNo = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + (int) (Math.random() * 90000 + 10000);
		orderDO.setOrderNumber(outTradeNo);
		financeService.save(orderDO);
		return R.ok();
	}
	@Autowired
	UserService userService;
	@Autowired
	UserDao userDao;

	//后台手动拨币

	/**
	 * 后台手动给用户拨币
	 * 输入正数 为该用户加积分
	 * 输入负数 为该用户减积分
	 * 判断用户积分不能为负数
	 * @param orderDO
	 * @return
	 */
	@PostMapping("/saveOrder")
	@Log("后台拨币")
	@ResponseBody
	@Transactional
	public R savehoutai(OrderDO orderDO) {
		UserDO userDO=userService.getByCode(orderDO.getCode());
		if(null==userDO){
			return R.error("用户不存在");
		}
		if (orderDO.getMoney() < 0D) {
			if ((orderDO.getMoney()+userDO.getBalance())<0){
				return R.error("该用户余额不足");
			}
		}

		orderDO.setStatus("1");
		orderDO.setFromUsername(userDO.getName());
		orderDO.setUserId(Integer.valueOf(userDO.getUserId().toString()));
		orderDO.setCreatTime(new Date());
		//0已付款未确定；1已付款已确定
		String outTradeNo = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + (int) (Math.random() * 90000 + 10000);
		orderDO.setOrderNumber(outTradeNo);
		financeService.save(orderDO);
		userDao.updateBalance(orderDO.getMoney(),userDO.getUserId());
		moneyHistoryService.addMoney(userDO.getUserId(), 0d, orderDO.getMoney(), 1L, "拨币", "b", userDO.getMoney() + "", userDO.getBalance()+orderDO.getMoney() + "");
		return R.ok();
	}

	/**
	 * 获取充值订单列表
	 * @param params
	 * @return
	 */
	@GetMapping("/get")
	@ResponseBody
	@Log("获取充值订单列表")
	PageUtils list(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		System.out.println(getUser());
		Query query = new Query(params);
		List<OrderDO> sysUserList = financeService.list(query);
		int total = financeService.count(query);
		PageUtils pageUtil = new PageUtils(sysUserList, total);
		return pageUtil;
	}

	@Autowired
	CurrencyHistoryService currencyHistoryService;

	/**
	 * 获取收益列表
	 * @param params
	 * @return
	 */
	@GetMapping("/getCurrent")
	@ResponseBody
	@Log("查看收益列表")
	PageUtils listCurrent(@RequestParam Map<String, Object> params) {
		// 查询列表数据
		if(getUserId()==149||getUserId()==148){
			params.remove("userId");
		}else{
			params.put("page", 1);
		}
		Query query = new Query(params);
		List<Map<String, Object>> sysUserList = currencyHistoryService.list(query);
		int total = currencyHistoryService.count(query);
		if(getUserId() == 149 || getUserId() == 148){
			PageUtils pageUtil = new PageUtils(sysUserList, total);
			return pageUtil;
		}else{
			PageUtils pageUtil = new PageUtils(sysUserList, 10);
			return pageUtil;
		}

	}

	@RequestMapping("/core")
	@ResponseBody
	public R core(Long userId,Double money){
		//financeService.core(userId,money);
		return R.ok();
	}

}
