package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 代理商
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-15 17:59:34
 */
public class ShopagentuserDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//
	private String agenttype;
	//
	private String company;
	//
	private String phone;
	//
	private Double money;
	//
	private String officename;
	//
	private Integer userid;
	//
	private String area;
	//
	private Integer code;
	//
	private Integer status;
	//
	private Date createtime;

	private String invitecode;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setAgenttype(String agenttype) {
		this.agenttype = agenttype;
	}
	/**
	 * 获取：
	 */
	public String getAgenttype() {
		return agenttype;
	}
	/**
	 * 设置：
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * 获取：
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * 设置：
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * 获取：
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * 设置：
	 */
	public void setOfficename(String officename) {
		this.officename = officename;
	}
	/**
	 * 获取：
	 */
	public String getOfficename() {
		return officename;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setArea(String area) {
		this.area = area;
	}
	/**
	 * 获取：
	 */
	public String getArea() {
		return area;
	}
	/**
	 * 设置：
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * 获取：
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatetime() {
		return createtime;
	}

	public String getInvitecode() {
		return invitecode;
	}

	public void setInvitecode(String invitecode) {
		this.invitecode = invitecode;
	}
}
