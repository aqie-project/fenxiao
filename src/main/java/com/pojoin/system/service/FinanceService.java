package com.pojoin.system.service;

import com.pojoin.system.domain.OrderDO;
import com.pojoin.system.domain.UserDO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;



public interface FinanceService {

    public int save(OrderDO orderDO);

    List<OrderDO> list(Map<String, Object> map);

    int count(Map<String, Object> map);
    public void core(UserDO newUser, UserDO inviteUser, UserDO baoDan, Long userId, Double money);

    public void saveMoney(Double recommend, Double award, Double range, Double profit, UserDO inviteUser, UserDO newUser);
/**************************************************************
 *          365元 3650元  36500元  10万元
 * 会员级别  商家    二级代理    一级代理    市代
 *                 4‰    3.2‰   2‰
 * 分享  15%
 *
 * 会员等级
 *    一星       3个商家     10%
 *    二星       10个商家    15%
 *    三星       50个商家    18%
 *    四星       100个商家   20%
 *    五星       300个商家   22%
 *    合伙人   两条线分别有一个五星
 *    董事    两条线分别有一个合伙人
 *
 *收益：一代收入的35%
 *     二代收入的35%
 *
 * 服务费：(注册)5%
 *
 *
 *推荐一个商家==一个会员
 *推荐一个二级代理==10个会员
 *推荐一个一级代理==100个会员
 *
 * 报单中心拥有注册权
 * 报单中心先向公司交钱，公司收到钱后，向报单中心拨积分   1元=一个积分
 * 报单中心注册一个商家 扣除该报单中心 365个积分
 * 报单中心注册一个二级代理 扣除该报单中心 3650个积分
 * 报单中心注册一个一级代理 扣除该报单中心 36500个积分
 *
 * 商家交365元，公司为该商家开通账号，商家可以享受分享 佣金 15%
 * 二级代理交3650元，公司为该二级代理开通代理账号，二级代理若注册用户，需向公司交钱，然后公司拨积分给该二级代理
 * 一级代理交3650元，公司为该一级代理开通代理账号，一级代理若注册用户，需向公司交钱，然后公司拨积分给该一级代理
 * 市代交10万代理资格，公司为该市代发10万的报单积分
 *
 *
 *
 *
 *
 *
 *                             1董事
 *                        /     |    \
 *                 2合伙人    3五星     4合伙人
 *                    /
 *              5三星（一级代理）
 *                   |
 *              6无（商家）
 *
 * 5推荐6
 * 6 交365元
 * 5 得  分享奖+等级奖+注册费 365*15%+365*18%+365*5%
 * 2 得  级差奖+一代收益+合伙人分红     365*4%+365*(15%+18%)*35%+365*5%/2
 * 1 得  二代收益+董事分红             365*(15%+18%)*35%+365*5%/1
 *
 *
 * 系统中的董事平分新加入用户所交费用的5%
 * 系统中的合伙人平分新加入用户所交费用的5%
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */


}
