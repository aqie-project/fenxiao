package com.pojoin.system.controller;

import com.pojoin.common.annotation.Log;
import com.pojoin.common.controller.BaseController;
import com.pojoin.common.domain.Tree;
import com.pojoin.common.service.DictService;
import com.pojoin.common.utils.MD5Utils;
import com.pojoin.common.utils.PageUtils;
import com.pojoin.common.utils.Query;
import com.pojoin.common.utils.R;
import com.pojoin.system.domain.DeptDO;
import com.pojoin.system.domain.RoleDO;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.service.RoleService;
import com.pojoin.system.service.UserService;
import com.pojoin.system.vo.UserVO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/sys/sys")
@Controller
public class SysController extends BaseController {
	private String prefix="system/setup"  ;

	@Log("系统参数设置")
	@GetMapping("/add")
	String add(Model model) {
		return prefix + "/add";
	}
}
