package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;


/**
 * 收益记录
 */
public class CurrencyHistoryDO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Double recommend;//推荐奖
    private Double award;//报单奖
    private Double range;//极差奖
    private Double profit;//代收益
    private Double bonus;//分红
    private Long userId;
    private Date creatTime;
    private String status;
    private Long  fromUserId;
    private String fromUserName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getRecommend() {
        return recommend;
    }

    /**
     * 推荐奖
     * @param recommend
     */
    public void setRecommend(Double recommend) {
        this.recommend = recommend;
    }

    public Double getAward() {
        return award;
    }

    /**
     * 报单奖
     * @param award
     */
    public void setAward(Double award) {
        this.award = award;
    }

    public Double getRange() {
        return range;
    }

    /**
     * 极差奖
     * @param range
     */
    public void setRange(Double range) {
        this.range = range;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Double getProfit() {
        return profit;
    }

    /**
     * 收益
     * @param profit
     */
    public void setProfit(Double profit) {
        this.profit = profit;
    }
}
