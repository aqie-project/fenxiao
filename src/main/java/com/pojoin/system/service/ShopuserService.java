package com.pojoin.system.service;

import com.pojoin.system.domain.ShopuserDO;
import com.pojoin.system.domain.UserDO;

import java.util.List;
import java.util.Map;

/**
 * 用户表
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-15 17:59:35
 */
public interface ShopuserService {
	
	ShopuserDO get(Integer id);
	
	List<ShopuserDO> list(Map<String, Object> map);

	ShopuserDO getByPhone(String phone);
	
	int count(Map<String, Object> map);
	
	int save(ShopuserDO shopuser);
	
	int update(ShopuserDO shopuser);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	int save(UserDO userDO);


}
