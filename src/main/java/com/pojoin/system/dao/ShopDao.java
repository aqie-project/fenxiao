package com.pojoin.system.dao;

import com.pojoin.system.domain.ShopDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 商店表
 * @author pojoin
 * @email 
 * @date 2020-07-16 11:16:31
 */
@Mapper
public interface ShopDao {

	ShopDO get(Integer id);
	
	List<ShopDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ShopDO shop);
	
	int update(ShopDO shop);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
