package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 现金转积分
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-13 10:33:00
 */
public class ChangeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//
	private Double money;
	//
	private Long userId;
	//
	private Double balance;
	//
	private Long creatId;
	//
	private Date creatTime;
	//
	private String bank1;
	//
	private String bank2;

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * 获取：
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * 设置：
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	/**
	 * 获取：
	 */
	public Double getBalance() {
		return balance;
	}
	/**
	 * 设置：
	 */
	public void setCreatId(Long creatId) {
		this.creatId = creatId;
	}
	/**
	 * 获取：
	 */
	public Long getCreatId() {
		return creatId;
	}
	/**
	 * 设置：
	 */
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatTime() {
		return creatTime;
	}
	/**
	 * 设置：
	 */
	public void setBank1(String bank1) {
		this.bank1 = bank1;
	}
	/**
	 * 获取：
	 */
	public String getBank1() {
		return bank1;
	}
	/**
	 * 设置：
	 */
	public void setBank2(String bank2) {
		this.bank2 = bank2;
	}
	/**
	 * 获取：
	 */
	public String getBank2() {
		return bank2;
	}
}
