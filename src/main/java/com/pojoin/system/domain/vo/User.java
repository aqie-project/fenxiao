package com.pojoin.system.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: aqie
 * @create: 2020-10-11 18:09
 **/
@Data
public class User implements Serializable {
    private Long userId;

    private String grade;

    private String username;

    private Long parentId;
}
