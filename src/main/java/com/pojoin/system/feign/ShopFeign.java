package com.pojoin.system.feign;

import com.pojoin.system.domain.ShopDO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @version 2.0.0
 * @auther liyangzhu
 * @since 2.0.0 2020/9/4
 */

@FeignClient(name = "SHOP", path = "/shop")
public interface ShopFeign {
    @RequestMapping("/shop/shop/registershop")
     void addShop(ShopDO shopDO);
}
