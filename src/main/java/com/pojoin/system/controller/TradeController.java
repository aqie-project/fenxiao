package com.pojoin.system.controller;

import java.util.List;
import java.util.Map;

import com.pojoin.common.annotation.Log;
import com.pojoin.common.controller.BaseController;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pojoin.system.domain.TradeDO;
import com.pojoin.system.service.TradeService;
import com.pojoin.common.utils.PageUtils;
import com.pojoin.common.utils.Query;
import com.pojoin.common.utils.R;

/**
 * 现金转增交易
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-13 10:33:00
 */
 
@Controller
@RequestMapping("/system/trade")
public class TradeController extends BaseController {
	@Autowired
	private TradeService tradeService;
	@Autowired
	private UserService userService;
	
	@GetMapping()
	@RequiresPermissions("system:trade:trade")
	String Trade(){
	    return "system/trade/trade";
	}

	@GetMapping("/trade1")
	@RequiresPermissions("system:trade:trade")
	String Trade1() {
		return "system/trade/trade1";
	}
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:trade:trade")
	@Log("转入列表")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
		UserDO userDO=userService.get(getUserId());
		params.put("userId", userDO.getUserId());
        Query query = new Query(params);
		List<TradeDO> tradeList = tradeService.list(query);
		int total = tradeService.count(query);
		PageUtils pageUtils = new PageUtils(tradeList, total);
		return pageUtils;
	}

	@ResponseBody
	@GetMapping("/list1")
	@RequiresPermissions("system:trade:trade")
	@Log("转出列表")
	public PageUtils list1(@RequestParam Map<String, Object> params) {
		//查询列表数据
		UserDO userDO = userService.get(getUserId());
		params.put("touserId", userDO.getUserId());
		Query query = new Query(params);
		List<TradeDO> tradeList = tradeService.list(query);
		int total = tradeService.count(query);
		PageUtils pageUtils = new PageUtils(tradeList, total);
		return pageUtils;
	}
	@GetMapping("/add")
	@RequiresPermissions("system:trade:add")
	String add(Model model){
		Double money = userService.get(getUserId()).getMoney();
		model.addAttribute("cash", money);
	    return "system/trade/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:trade:edit")
	String edit(@PathVariable("id") Long id,Model model){
		TradeDO trade = tradeService.get(id);
		model.addAttribute("trade", trade);
	    return "system/trade/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:trade:add")
	@Log("保存转账记录")
	public R save( TradeDO trade){
		UserDO userDO=userService.getByCode(trade.getBank2());
		if(null==userDO){
			return R.error("用户不存在！");
		}
		if(userDO.getLevel().equals("0")){
			return R.error("该编号不是报单中心");
		}
		UserDO from=userService.get(getUserId());
		if(from.getMoney()<trade.getMoney()){
			return R.error("余额不足");
		}
		trade.setBank1(from.getCode());
		trade.setTouserId(userDO.getUserId());
		trade.setBank2(userDO.getCode());
		trade.setCreatId(getUserId());
		trade.setUserId(getUserId());
		if(tradeService.save(trade)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:trade:edit")
	public R update( TradeDO trade){
		tradeService.update(trade);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:trade:remove")
	public R remove( Long id){
		if(tradeService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:trade:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		tradeService.batchRemove(ids);
		return R.ok();
	}

	@GetMapping("/getHoutai")
	@RequiresPermissions("system:trade:trade")
	String TradeHoutai() {
		return "system/trade/tradeHoutai";
	}

	@ResponseBody
	@GetMapping("/listHoutai")
	@Log("后台转账列表")
	public PageUtils listHoutai(@RequestParam Map<String, Object> params) {
		//查询列表数据
		Query query = new Query(params);
		List<Map<String,Object>> tradeList = tradeService.listHoutai(query);
		int total = tradeService.countHoutai(query);
		PageUtils pageUtils = new PageUtils(tradeList, total);
		return pageUtils;
	}
}
