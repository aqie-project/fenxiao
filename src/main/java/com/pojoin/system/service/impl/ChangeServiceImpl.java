package com.pojoin.system.service.impl;

import com.pojoin.system.dao.MoneyHistoryDao;
import com.pojoin.system.dao.UserDao;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.service.MoneyHistoryService;
import com.pojoin.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.pojoin.system.dao.ChangeDao;
import com.pojoin.system.domain.ChangeDO;
import com.pojoin.system.service.ChangeService;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ChangeServiceImpl implements ChangeService {
	@Autowired
	private ChangeDao changeDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private MoneyHistoryService moneyHistoryService;

	@Override
	public ChangeDO get(Long id){
		return changeDao.get(id);
	}
	
	@Override
	public List<ChangeDO> list(Map<String, Object> map){
		return changeDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return changeDao.count(map);
	}

	/**
	 * 现金转积分
	 * @param change
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int save(ChangeDO change){
		change.setBank2("");
		change.setCreatTime(new Date());
		change.setBalance(change.getMoney());
		userDao.updateMoney(-change.getMoney(),change.getUserId());
		userDao.updateBalance(change.getMoney(),change.getUserId());
		UserDO userDO=userDao.get(change.getUserId());
		moneyHistoryService.addMoney(change.getUserId(),-change.getMoney(),0d,change.getUserId(),"转积分","m", userDO.getMoney().toString(), userDO.getBalance().toString());
		moneyHistoryService.addMoney(change.getUserId(), 0d, change.getMoney(), change.getUserId(), "转积分", "b", userDO.getMoney().toString(), userDO.getBalance().toString());
		return changeDao.save(change);
	}
	
	@Override
	public int update(ChangeDO change){
		return changeDao.update(change);
	}
	
	@Override
	public int remove(Long id){
		return changeDao.remove(id);
	}
	
	@Override
	public int batchRemove(Long[] ids){
		return changeDao.batchRemove(ids);
	}

	@Override
	public List<Map<String, Object>> listHoutai(Map<String, Object> params) {
		return changeDao.listHoutai(params);
	}

	@Override
	public int countHoutai(Map<String, Object> params) {
		return changeDao.countHoutai(params);
	}

}
