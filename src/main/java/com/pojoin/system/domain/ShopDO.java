package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 商店表
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-16 11:16:31
 */
public class ShopDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//封面图片
	private String logo;
	//店名
	private String name;
	//描述
	private String desc;
	//电话
	private String cellphone;
	//地址
	private String address;
	//经度
	private String ypoint;
	//纬度
	private String xpoint;
	//高级商家还是普通商家
	private String shoptype;
	//
	private String category;
	//
	private String city;
	//
	private Integer sortnum;
	//
	private Date createtime;
	//
	private Integer userid;
	//渠道商邀请码
	private String invitecode;
	//折扣比例
	private String discount;
	//负责人
	private String username;
	//
	private Integer ishasbusness;
	//
	private String evaluatescore;
	//
	private Integer status;
	//
	private String location;
	//
	private String parentcategory;
	//日限额
	private Double maxdaymoney;
	//折扣上限
	private Double maxdiscount;
	//店铺图标
	private String icon;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：封面图片
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}
	/**
	 * 获取：封面图片
	 */
	public String getLogo() {
		return logo;
	}
	/**
	 * 设置：店名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：店名
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：描述
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * 获取：描述
	 */
	public String getDesc() {
		return desc;
	}
	/**
	 * 设置：电话
	 */
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}
	/**
	 * 获取：电话
	 */
	public String getCellphone() {
		return cellphone;
	}
	/**
	 * 设置：地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：经度
	 */
	public void setYpoint(String ypoint) {
		this.ypoint = ypoint;
	}
	/**
	 * 获取：经度
	 */
	public String getYpoint() {
		return ypoint;
	}
	/**
	 * 设置：纬度
	 */
	public void setXpoint(String xpoint) {
		this.xpoint = xpoint;
	}
	/**
	 * 获取：纬度
	 */
	public String getXpoint() {
		return xpoint;
	}
	/**
	 * 设置：高级商家还是普通商家
	 */
	public void setShoptype(String shoptype) {
		this.shoptype = shoptype;
	}
	/**
	 * 获取：高级商家还是普通商家
	 */
	public String getShoptype() {
		return shoptype;
	}
	/**
	 * 设置：
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * 获取：
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * 设置：
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * 获取：
	 */
	public String getCity() {
		return city;
	}
	/**
	 * 设置：
	 */
	public void setSortnum(Integer sortnum) {
		this.sortnum = sortnum;
	}
	/**
	 * 获取：
	 */
	public Integer getSortnum() {
		return sortnum;
	}
	/**
	 * 设置：
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatetime() {
		return createtime;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：渠道商邀请码
	 */
	public void setInvitecode(String invitecode) {
		this.invitecode = invitecode;
	}
	/**
	 * 获取：渠道商邀请码
	 */
	public String getInvitecode() {
		return invitecode;
	}
	/**
	 * 设置：折扣比例
	 */
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	/**
	 * 获取：折扣比例
	 */
	public String getDiscount() {
		return discount;
	}
	/**
	 * 设置：负责人
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * 获取：负责人
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * 设置：
	 */
	public void setIshasbusness(Integer ishasbusness) {
		this.ishasbusness = ishasbusness;
	}
	/**
	 * 获取：
	 */
	public Integer getIshasbusness() {
		return ishasbusness;
	}
	/**
	 * 设置：
	 */
	public void setEvaluatescore(String evaluatescore) {
		this.evaluatescore = evaluatescore;
	}
	/**
	 * 获取：
	 */
	public String getEvaluatescore() {
		return evaluatescore;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * 获取：
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * 设置：
	 */
	public void setParentcategory(String parentcategory) {
		this.parentcategory = parentcategory;
	}
	/**
	 * 获取：
	 */
	public String getParentcategory() {
		return parentcategory;
	}
	/**
	 * 设置：日限额
	 */
	public void setMaxdaymoney(Double maxdaymoney) {
		this.maxdaymoney = maxdaymoney;
	}
	/**
	 * 获取：日限额
	 */
	public Double getMaxdaymoney() {
		return maxdaymoney;
	}
	/**
	 * 设置：折扣上限
	 */
	public void setMaxdiscount(Double maxdiscount) {
		this.maxdiscount = maxdiscount;
	}
	/**
	 * 获取：折扣上限
	 */
	public Double getMaxdiscount() {
		return maxdiscount;
	}
	/**
	 * 设置：店铺图标
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	/**
	 * 获取：店铺图标
	 */
	public String getIcon() {
		return icon;
	}
}
