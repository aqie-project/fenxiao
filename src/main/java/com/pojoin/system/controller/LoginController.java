package com.pojoin.system.controller;

import com.pojoin.common.controller.BaseController;
import com.pojoin.common.domain.FileDO;
import com.pojoin.common.domain.Tree;
import com.pojoin.common.service.FileService;
import com.pojoin.common.utils.MD5Utils;
import com.pojoin.common.utils.R;
import com.pojoin.common.utils.ShiroUtils;
import com.pojoin.system.domain.MenuDO;
import com.pojoin.system.service.MenuService;
import com.pojoin.system.service.OrderService;
import com.pojoin.system.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	MenuService menuService;
	@Autowired
	FileService fileService;
	@Autowired
	UserService userService;

	@GetMapping({ "/", "" })
	String welcome(Model model) {

		return "redirect:/index";
	}

	@GetMapping({ "/index" })
	String index(Model model) {
		List<Tree<MenuDO>> menus = menuService.listMenuTree(getUserId());
		model.addAttribute("menus", menus);
		model.addAttribute("name", getUser().getName());
		FileDO fileDO = fileService.get(getUser().getPicId());
		if(fileDO!=null&&fileDO.getUrl()!=null){
			if(fileService.isExist(fileDO.getUrl())){
				model.addAttribute("picUrl",fileDO.getUrl());
			}else {
				model.addAttribute("picUrl","/img/hui.jpg");
			}
		}else {
			model.addAttribute("picUrl","/img/hui.jpg");
		}
		model.addAttribute("username", getUser().getUsername());




		return "index_v1";
	}

	@GetMapping("/login")
	String login() {
		return "login";
	}

	//@Log("登录")
	@PostMapping("/login")
	@ResponseBody
	R ajaxLogin(String username, String password) {
		password = MD5Utils.encrypt(username, password);
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		Subject subject = SecurityUtils.getSubject();
		try {
			subject.login(token);
			return R.ok();
		} catch (AuthenticationException e) {
			return R.error("用户或密码错误");
		}
	}

	@GetMapping("/logout")
	String logout() {
		ShiroUtils.logout();
		return "redirect:/login";
	}
	@Autowired
	OrderService orderService;
	@GetMapping("/main")
	String main(Model model) {
		model.addAttribute("user",userService.get(getUserId()));
		model.addAttribute("num",userService.selectSumUser());
		if("0".equals(getUser().getLevel())){
			model.addAttribute("level", "商家");
		}
		if ("1".equals(getUser().getLevel())) {
			model.addAttribute("level", "二级代理");
		}
		if ("2".equals(getUser().getLevel())) {
			model.addAttribute("level", "一级代理");

		}
		if ("3".equals(getUser().getLevel())) {
			model.addAttribute("level", "市代");

		}
		Map<String,Object> map=new HashMap<>();
		map.put("invitateCode",getUser().getCode());
		model.addAttribute("zhitui", userService.count(map));

		if(getUserId()==149){
			model.addAttribute("sumMoney", userService.selectSumMoney());
			model.addAttribute("today", userService.selectTodayNum());
			model.addAttribute("todayMoney", orderService.selectTodayMoney());
			model.addAttribute("u0", userService.selectMemUserByLevel("0"));
			model.addAttribute("u1", userService.selectMemUserByLevel("1"));
			model.addAttribute("u2", userService.selectMemUserByLevel("2"));
			model.addAttribute("u3", userService.selectMemUserByLevel("3"));
			model.addAttribute("wu", userService.selectMemUserByGrade("无"));
			model.addAttribute("one",userService.selectMemUserByGrade("一星"));
			model.addAttribute("two", userService.selectMemUserByGrade("二星"));
			model.addAttribute("three", userService.selectMemUserByGrade("三星"));
			model.addAttribute("four", userService.selectMemUserByGrade("四星"));
			model.addAttribute("five", userService.selectMemUserByGrade("五星"));
			model.addAttribute("six", userService.selectMemUserByGrade("合伙人"));
			model.addAttribute("seven", userService.selectMemUserByGrade("董事"));
		}


		return "main";
	}

}
