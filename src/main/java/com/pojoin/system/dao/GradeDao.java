package com.pojoin.system.dao;

import com.pojoin.system.domain.GradeDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author pojoin
 * @email 
 * @date 2020-06-30 16:25:00
 */
@Mapper
public interface GradeDao {

	GradeDO get(Integer id);
	
	List<GradeDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(GradeDO grade);
	
	int update(GradeDO grade);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	/**
	 * 根据等级名称查询等级
	 * @param name
	 * @return
	 */
	GradeDO getByGrade(@Param("gradeName") String gradeName);
}
