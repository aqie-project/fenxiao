package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 渠道商表
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-15 17:59:35
 */
public class ShopchanneluserDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Integer id;
	//
	private String username;
	//
	private String phone;
	//
	private String remark;
	//
	private String invitecode;
	//
	private String identcard;
	//
	private Integer userid;
	//
	private String address;
	//
	private Integer code;
	//
	private Integer status;
	//
	private Date createtime;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * 获取：
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * 设置：
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：
	 */
	public void setInvitecode(String invitecode) {
		this.invitecode = invitecode;
	}
	/**
	 * 获取：
	 */
	public String getInvitecode() {
		return invitecode;
	}
	/**
	 * 设置：
	 */
	public void setIdentcard(String identcard) {
		this.identcard = identcard;
	}
	/**
	 * 获取：
	 */
	public String getIdentcard() {
		return identcard;
	}
	/**
	 * 设置：
	 */
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/**
	 * 获取：
	 */
	public Integer getUserid() {
		return userid;
	}
	/**
	 * 设置：
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * 获取：
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * 设置：
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatetime() {
		return createtime;
	}
}
