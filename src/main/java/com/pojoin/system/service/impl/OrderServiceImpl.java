package com.pojoin.system.service.impl;

import com.pojoin.system.dao.CurrencyHistoryDao;
import com.pojoin.system.domain.CurrencyHistoryDO;
import com.pojoin.system.domain.UserDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.pojoin.system.dao.OrderDao;
import com.pojoin.system.domain.OrderDO;
import com.pojoin.system.service.OrderService;



@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderDao orderDao;
	@Autowired
	CurrencyHistoryDao currencyHistoryDao;
	
	@Override
	public OrderDO get(Integer id){
		return orderDao.get(id);
	}
	
	@Override
	public List<OrderDO> list(Map<String, Object> map){
		return orderDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return orderDao.count(map);
	}
	
	@Override
	public int save(OrderDO order){
		return orderDao.save(order);
	}
	
	@Override
	public int update(OrderDO order){
		return orderDao.update(order);
	}
	
	@Override
	public int remove(Integer id){
		return orderDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return orderDao.batchRemove(ids);
	}

	@Override
	public void merge(UserDO newUser) {
		        List<CurrencyHistoryDO>userIds=currencyHistoryDao.getByFromUserId(newUser.getUserId());
        if(null!=userIds&&userIds.size()>0){
            for(CurrencyHistoryDO currencyHistoryDO:userIds){
                List<CurrencyHistoryDO>currency=currencyHistoryDao.getByFromUserIdAndUserId(newUser.getUserId(),currencyHistoryDO.getUserId());
                CurrencyHistoryDO currencyHistoryDO2 = new CurrencyHistoryDO();
                for(CurrencyHistoryDO currencyHistoryDO1:currency){

                    if(currencyHistoryDO1.getAward()>0d){
                        currencyHistoryDO2.setAward(currencyHistoryDO1.getAward());
                    }
                    if(currencyHistoryDO1.getBonus()>0d){
                        currencyHistoryDO2.setBonus(currencyHistoryDO1.getBonus());
                    }
                    if(currencyHistoryDO1.getProfit()>0d){
                        currencyHistoryDO2.setProfit(currencyHistoryDO1.getProfit());
                    }
                    if(currencyHistoryDO1.getRange()>0d){
                        currencyHistoryDO2.setRange(currencyHistoryDO1.getRange());
                    }
                    if(currencyHistoryDO1.getRecommend()>0d){
                        currencyHistoryDO2.setRecommend(currencyHistoryDO1.getRecommend());
                    }
                    currencyHistoryDO2.setFromUserName(currencyHistoryDO1.getFromUserName());
                    currencyHistoryDO2.setStatus(currencyHistoryDO1.getStatus());
                    currencyHistoryDO2.setUserId(currencyHistoryDO1.getUserId());
                    currencyHistoryDO2.setCreatTime(new Date());
                    currencyHistoryDO2.setFromUserId(currencyHistoryDO1.getFromUserId());
                    currencyHistoryDO1.setStatus("2");
                    currencyHistoryDao.update(currencyHistoryDO1);
                }
                currencyHistoryDao.save(currencyHistoryDO2);
            }
        }
	}

	@Override
	public Double selectTodayMoney() {
		return orderDao.selectTodayMoney();
	}


}
