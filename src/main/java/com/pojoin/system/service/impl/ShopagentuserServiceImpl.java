package com.pojoin.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.pojoin.system.dao.ShopagentuserDao;
import com.pojoin.system.domain.ShopagentuserDO;
import com.pojoin.system.service.ShopagentuserService;



@Service
public class ShopagentuserServiceImpl implements ShopagentuserService {
	@Autowired
	private ShopagentuserDao shopagentuserDao;
	
	@Override
	public ShopagentuserDO get(Integer id){
		return shopagentuserDao.get(id);
	}
	
	@Override
	public List<ShopagentuserDO> list(Map<String, Object> map){
		return shopagentuserDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return shopagentuserDao.count(map);
	}
	
	@Override
	public int save(ShopagentuserDO shopagentuser){
		return shopagentuserDao.save(shopagentuser);
	}
	
	@Override
	public int update(ShopagentuserDO shopagentuser){
		return shopagentuserDao.update(shopagentuser);
	}
	
	@Override
	public int remove(Integer id){
		return shopagentuserDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return shopagentuserDao.batchRemove(ids);
	}
	
}
