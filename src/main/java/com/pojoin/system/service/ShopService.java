package com.pojoin.system.service;

import com.pojoin.system.domain.ShopDO;

import java.util.List;
import java.util.Map;

/**
 * 商店表
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-16 11:16:31
 */
public interface ShopService {
	
	ShopDO get(Integer id);
	
	List<ShopDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ShopDO shop);
	
	int update(ShopDO shop);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
