package com.pojoin.system.dao;

import com.pojoin.system.domain.MoneyHistoryDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 资金流水记录
 * @author pojoin
 * @email 
 * @date 2020-07-27 16:44:47
 */
@Mapper
public interface MoneyHistoryDao {

	MoneyHistoryDO get(Integer id);
	
	List<MoneyHistoryDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(MoneyHistoryDO moneyHistory);
	
	int update(MoneyHistoryDO moneyHistory);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
