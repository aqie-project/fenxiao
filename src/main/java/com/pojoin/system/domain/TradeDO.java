package com.pojoin.system.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 现金转增交易
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-13 10:33:00
 */
public class TradeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
	//
	private Long userId;
	//
	private Long touserId;
	//
	private Double money;
	//
	private Long creatId;
	//
	private Date creatTime;
	//1:转入；2：转出
	private String status;
	//
	private String bank1;
	//
	private String bank2;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：
	 */
	public void setTouserId(Long touserId) {
		this.touserId = touserId;
	}
	/**
	 * 获取：
	 */
	public Long getTouserId() {
		return touserId;
	}
	/**
	 * 设置：
	 */
	public void setMoney(Double money) {
		this.money = money;
	}
	/**
	 * 获取：
	 */
	public Double getMoney() {
		return money;
	}
	/**
	 * 设置：
	 */
	public void setCreatId(Long creatId) {
		this.creatId = creatId;
	}
	/**
	 * 获取：
	 */
	public Long getCreatId() {
		return creatId;
	}
	/**
	 * 设置：
	 */
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreatTime() {
		return creatTime;
	}
	/**
	 * 设置：1:转入；2：转出
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：1:转入；2：转出
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：
	 */
	public void setBank1(String bank1) {
		this.bank1 = bank1;
	}
	/**
	 * 获取：
	 */
	public String getBank1() {
		return bank1;
	}
	/**
	 * 设置：
	 */
	public void setBank2(String bank2) {
		this.bank2 = bank2;
	}
	/**
	 * 获取：
	 */
	public String getBank2() {
		return bank2;
	}

}
