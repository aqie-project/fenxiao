package com.pojoin.system.dao;

import com.pojoin.system.domain.CurrencyHistoryDO;
import com.pojoin.system.domain.DeptDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface CurrencyHistoryDao {

    CurrencyHistoryDO get(Integer id);

    List<Map<String,Object>> list(Map<String, Object> map);

    int count(Map<String, Object> map);

    int save(CurrencyHistoryDO currencyHistory);

    int update(CurrencyHistoryDO currencyHistory);

    int remove(Integer id);

    int batchRemove(Integer[] ids);

    List<CurrencyHistoryDO> getByFromUserId(Long userId);

    List<CurrencyHistoryDO> getByFromUserIdAndUserId(@Param("fromUserId")Long fromUserId,@Param("userId")Long userId);

}
