package com.pojoin.system.service;

import com.pojoin.system.domain.ChangeDO;

import java.util.List;
import java.util.Map;

/**
 * 现金转积分
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-13 10:33:00
 */
public interface ChangeService {
	
	ChangeDO get(Long id);
	
	List<ChangeDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ChangeDO change);
	
	int update(ChangeDO change);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

	List<Map<String, Object>> listHoutai(Map<String, Object> params);

	int countHoutai(Map<String, Object> params);
}
