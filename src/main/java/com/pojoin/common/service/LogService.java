package com.pojoin.common.service;

import org.springframework.stereotype.Service;

import com.pojoin.common.domain.LogDO;
import com.pojoin.common.domain.PageDO;
import com.pojoin.common.utils.Query;
@Service
public interface LogService {
	void save(LogDO logDO);
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
