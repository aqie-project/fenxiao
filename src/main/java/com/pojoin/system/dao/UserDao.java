package com.pojoin.system.dao;

import com.pojoin.system.domain.vo.User;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.domain.vo.UserGradeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author chglee
 * @date 2017-10-03 09:45:11
 */
@Mapper
public interface UserDao {

	UserDO get(Long userId);

	UserDO getByInvited(String code);

	UserDO getByCore(String code);

	/**
	 * 获取我的团队数量
	 * @param userId
	 * @return
	 */
	int countMyTeam(Long userId);

	List<UserDO> list(Map<String,Object> map);

	List<UserDO> listTree(Map<String, Object> map);
	
	int count(Map<String,Object> map);

	int countPartner(Map<String,Object> map);

	int save(UserDO user);

	int insertData(List<UserDO> users);

	int update(UserDO user);
	
	int remove(Long userId);
	
	int batchRemove(Long[] userIds);
	
	Long[] listAllDept();

    UserDO selectByCode(String code);

	List<UserDO> listInvite(String parentLink);

	List<User> listInvite2(String parentLink);

	int updateBalance(@Param("balance") Double balance,@Param("userId") Long userId);

	int updateGrade(@Param("grade") String grade,@Param("userId") Long userId);

	int updateMoney(@Param("money") Double money, @Param("userId") Long userId);


	List<UserDO> listAdmin(Map<String, Object> map);


    List<UserDO> listAll(Map map);
    List<UserDO> listAllGradeLgFive();
	//查询系统合伙人
    List<UserDO>listH(Map map);
    //查询系统中的董事
    List<UserDO>listD();

    int selectTodayNum();

    int selectSumUser();

    int selectMemUserByLevel(String level);

    int selectMemUserByGrade(String grade);

    Double selectSumMoney();

    // 根据用户id 查询用户星级对应的百分比
	UserGradeVo selectUserGradeByUserId(Long userId);
}
