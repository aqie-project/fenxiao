package com.pojoin.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.pojoin.system.dao.ShopDao;
import com.pojoin.system.domain.ShopDO;
import com.pojoin.system.service.ShopService;



@Service
public class ShopServiceImpl implements ShopService {
	@Autowired
	private ShopDao shopDao;
	
	@Override
	public ShopDO get(Integer id){
		return shopDao.get(id);
	}
	
	@Override
	public List<ShopDO> list(Map<String, Object> map){
		return shopDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return shopDao.count(map);
	}
	
	@Override
	public int save(ShopDO shop){
		return shopDao.save(shop);
	}
	
	@Override
	public int update(ShopDO shop){
		return shopDao.update(shop);
	}
	
	@Override
	public int remove(Integer id){
		return shopDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return shopDao.batchRemove(ids);
	}
	
}
