package com.pojoin.system.service;

import com.pojoin.system.domain.TradeDO;

import java.util.List;
import java.util.Map;

/**
 * 现金转增交易
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-13 10:33:00
 */
public interface TradeService {
	
	TradeDO get(Long id);
	
	List<TradeDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(TradeDO trade);
	
	int update(TradeDO trade);
	
	int remove(Long id);
	
	int batchRemove(Long[] ids);

	List<Map<String, Object>> listHoutai(Map<String, Object> params);

	int countHoutai(Map<String, Object> params);
}
