package com.pojoin.system.service;

import com.pojoin.system.domain.MoneyHistoryDO;

import java.util.List;
import java.util.Map;

/**
 * 资金流水记录
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-27 15:56:44
 */
public interface MoneyHistoryService {
	
	MoneyHistoryDO get(Integer id);
	
	List<MoneyHistoryDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(MoneyHistoryDO moneyHistory);
	
	int update(MoneyHistoryDO moneyHistory);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	int addMoney(Long userId, Double money, Double balanc, Long fromUserId, String remark, String type,String moneyHistry,String balanbeHistory);
}
