﻿# Host: localhost  (Version: 5.7.26)
# Date: 2020-10-23 23:00:20
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "adminuser"
#

DROP TABLE IF EXISTS `adminuser`;
CREATE TABLE `adminuser` (
  `id` int(11) DEFAULT NULL,
  `PHONE` varchar(100) DEFAULT NULL COMMENT '手机号',
  `PASSWORD` varchar(100) DEFAULT NULL COMMENT '密码',
  `role_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='后台用户表';

#
# Data for table "adminuser"
#


#
# Structure for table "appcategory"
#

DROP TABLE IF EXISTS `appcategory`;
CREATE TABLE `appcategory` (
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='共享应用服务分类';

#
# Data for table "appcategory"
#


#
# Structure for table "applist"
#

DROP TABLE IF EXISTS `applist`;
CREATE TABLE `applist` (
  `category` varchar(255) DEFAULT NULL COMMENT '类型',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `url` varchar(255) DEFAULT NULL COMMENT '路径',
  `logo` varchar(255) DEFAULT NULL COMMENT '图标'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='共享应用';

#
# Data for table "applist"
#


#
# Structure for table "applist_copy"
#

DROP TABLE IF EXISTS `applist_copy`;
CREATE TABLE `applist_copy` (
  `category` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "applist_copy"
#


#
# Structure for table "blog_content"
#

DROP TABLE IF EXISTS `blog_content`;
CREATE TABLE `blog_content` (
  `cid` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `slug` varchar(255) DEFAULT NULL,
  `created` bigint(20) DEFAULT NULL COMMENT '创建人id',
  `modified` bigint(20) DEFAULT NULL COMMENT '最近修改人id',
  `content` text COMMENT '内容',
  `type` varchar(16) DEFAULT NULL COMMENT '类型',
  `tags` varchar(200) DEFAULT NULL COMMENT '标签',
  `categories` varchar(200) DEFAULT NULL COMMENT '分类',
  `hits` int(5) DEFAULT NULL,
  `comments_num` int(5) DEFAULT '0' COMMENT '评论数量',
  `allow_comment` int(1) DEFAULT '0' COMMENT '开启评论',
  `allow_ping` int(1) DEFAULT '0' COMMENT '允许ping',
  `allow_feed` int(1) DEFAULT '0' COMMENT '允许反馈',
  `status` int(1) DEFAULT NULL COMMENT '状态',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `gtm_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gtm_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`cid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章内容';

#
# Data for table "blog_content"
#


#
# Structure for table "change_order_history"
#

DROP TABLE IF EXISTS `change_order_history`;
CREATE TABLE `change_order_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL COMMENT '商品id',
  `product_name` varchar(200) NOT NULL COMMENT '商品名称',
  `integral` varchar(20) DEFAULT NULL COMMENT '积分值',
  `address` varchar(255) DEFAULT NULL COMMENT '兑换地址',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` tinyint(2) DEFAULT '0' COMMENT '0 未发货 1 已发货',
  `is_deleted` tinyint(2) DEFAULT '0' COMMENT '是否删除 1 删除',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `user_id` int(11) NOT NULL COMMENT '兑换人id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='兑换商品记录表';

#
# Data for table "change_order_history"
#


#
# Structure for table "comment"
#

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dataId` int(11) DEFAULT NULL COMMENT 'shopid',
  `content` text,
  `userId` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT '1000-10-10 10:10:01',
  `userName` varchar(255) DEFAULT NULL,
  `userPhoto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "comment"
#


#
# Structure for table "deliverycategory"
#

DROP TABLE IF EXISTS `deliverycategory`;
CREATE TABLE `deliverycategory` (
  `photo` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='同城帮四图';

#
# Data for table "deliverycategory"
#


#
# Structure for table "goodcategory"
#

DROP TABLE IF EXISTS `goodcategory`;
CREATE TABLE `goodcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商品分类';

#
# Data for table "goodcategory"
#


#
# Structure for table "oa_notify"
#

DROP TABLE IF EXISTS `oa_notify`;
CREATE TABLE `oa_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `title` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '标题',
  `content` varchar(2000) COLLATE utf8_bin DEFAULT NULL COMMENT '内容',
  `files` varchar(2000) COLLATE utf8_bin DEFAULT NULL COMMENT '附件',
  `status` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '状态',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oa_notify_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='通知通告';

#
# Data for table "oa_notify"
#


#
# Structure for table "oa_notify_record"
#

DROP TABLE IF EXISTS `oa_notify_record`;
CREATE TABLE `oa_notify_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `notify_id` bigint(20) DEFAULT NULL COMMENT '通知通告ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '接受人',
  `is_read` tinyint(1) DEFAULT '0' COMMENT '阅读标记',
  `read_date` date DEFAULT NULL COMMENT '阅读时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oa_notify_record_notify_id` (`notify_id`) USING BTREE,
  KEY `oa_notify_record_user_id` (`user_id`) USING BTREE,
  KEY `oa_notify_record_read_flag` (`is_read`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='通知通告发送记录';

#
# Data for table "oa_notify_record"
#


#
# Structure for table "preorder"
#

DROP TABLE IF EXISTS `preorder`;
CREATE TABLE `preorder` (
  `shopName` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `orderNumber` bigint(255) DEFAULT NULL,
  `prize` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `wxPay` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "preorder"
#


#
# Structure for table "preorder_bak_20190928"
#

DROP TABLE IF EXISTS `preorder_bak_20190928`;
CREATE TABLE `preorder_bak_20190928` (
  `shopName` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `orderNumber` bigint(255) DEFAULT NULL,
  `prize` varchar(255) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `wxPay` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "preorder_bak_20190928"
#


#
# Structure for table "preshop"
#

DROP TABLE IF EXISTS `preshop`;
CREATE TABLE `preshop` (
  `inviteCode` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `money` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `userPhone` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cellphone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "preshop"
#


#
# Structure for table "preuser"
#

DROP TABLE IF EXISTS `preuser`;
CREATE TABLE `preuser` (
  `邀请码` varchar(255) DEFAULT NULL,
  `会员号` varchar(255) DEFAULT NULL,
  `密码` varchar(255) DEFAULT NULL,
  `现金` varchar(255) DEFAULT NULL,
  `姓名` varchar(255) DEFAULT NULL,
  `手机号` varchar(255) DEFAULT NULL,
  `所在地区` varchar(255) DEFAULT NULL,
  `个人简介` varchar(255) DEFAULT NULL,
  `从事行业` varchar(255) DEFAULT NULL,
  `所属单位` varchar(255) DEFAULT NULL,
  `职称名称` varchar(255) DEFAULT NULL,
  `身份证号` varchar(255) DEFAULT NULL,
  `实名认证状态` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "preuser"
#


#
# Structure for table "preuser_bak20191003"
#

DROP TABLE IF EXISTS `preuser_bak20191003`;
CREATE TABLE `preuser_bak20191003` (
  `邀请码` varchar(255) DEFAULT NULL,
  `会员号` varchar(255) DEFAULT NULL,
  `密码` varchar(255) DEFAULT NULL,
  `现金` varchar(255) DEFAULT NULL,
  `姓名` varchar(255) DEFAULT NULL,
  `手机号` varchar(255) DEFAULT NULL,
  `所在地区` varchar(255) DEFAULT NULL,
  `个人简介` varchar(255) DEFAULT NULL,
  `从事行业` varchar(255) DEFAULT NULL,
  `所属单位` varchar(255) DEFAULT NULL,
  `职称名称` varchar(255) DEFAULT NULL,
  `身份证号` varchar(255) DEFAULT NULL,
  `实名认证状态` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "preuser_bak20191003"
#


#
# Structure for table "repetition_check"
#

DROP TABLE IF EXISTS `repetition_check`;
CREATE TABLE `repetition_check` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `money` varchar(100) NOT NULL COMMENT '金额',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_money_time` (`user_id`,`money`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5613 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='提现防重复';

#
# Data for table "repetition_check"
#


#
# Structure for table "shop"
#

DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` longtext COMMENT '封面图片',
  `name` longtext COMMENT '店名',
  `desc` longtext COMMENT '描述',
  `cellphone` longtext COMMENT '电话',
  `userId` int(11) DEFAULT NULL,
  `address` longtext COMMENT '地址',
  `ypoint` longtext COMMENT '经度',
  `xpoint` longtext COMMENT '纬度',
  `shopType` longtext COMMENT '高级商家还是普通商家',
  `category` longtext,
  `city` longtext,
  `sortNum` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `inviteCode` longtext COMMENT '渠道商邀请码',
  `discount` longtext COMMENT '折扣比例',
  `userName` longtext COMMENT '负责人',
  `isHasBusness` int(255) DEFAULT NULL,
  `evaluateScore` varchar(10) DEFAULT '5',
  `status` int(11) DEFAULT NULL,
  `location` longtext,
  `parentCategory` longtext,
  `maxDayMoney` double(20,2) DEFAULT '0.00' COMMENT '日限额',
  `maxDiscount` double(4,2) DEFAULT '10.00' COMMENT '折扣上限',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT 'http://www.hhlmcn.com/upload-img/default/shop-icon.png' COMMENT '店铺图标',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `inx_su_userId` (`userId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6436 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "shop"
#


#
# Structure for table "shop_goods_class"
#

DROP TABLE IF EXISTS `shop_goods_class`;
CREATE TABLE `shop_goods_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `userId` int(11) DEFAULT NULL COMMENT '用户ID',
  `shopId` int(11) DEFAULT NULL COMMENT '商铺ID',
  `status` int(2) DEFAULT '1' COMMENT '-1 = 已删除, 0 = 失效, 1 = 有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=744 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shop_goods_class"
#


#
# Structure for table "shop_nav_img"
#

DROP TABLE IF EXISTS `shop_nav_img`;
CREATE TABLE `shop_nav_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL COMMENT '图片地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "shop_nav_img"
#


#
# Structure for table "shop_order_item"
#

DROP TABLE IF EXISTS `shop_order_item`;
CREATE TABLE `shop_order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goodsId` bigint(20) NOT NULL COMMENT '商品id',
  `orderNo` varchar(100) DEFAULT NULL COMMENT '订单id',
  `prize` double(11,2) NOT NULL COMMENT '商品单价',
  `goodscount` int(10) NOT NULL COMMENT '购买数量',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2522 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单明细表';

#
# Data for table "shop_order_item"
#


#
# Structure for table "shopagentuser"
#

DROP TABLE IF EXISTS `shopagentuser`;
CREATE TABLE `shopagentuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agentType` varchar(255) DEFAULT NULL COMMENT '代理商类型',
  `company` varchar(255) DEFAULT NULL COMMENT '公司名称',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `money` double(11,2) DEFAULT NULL,
  `officeName` varchar(255) DEFAULT NULL COMMENT '办事处',
  `userId` int(11) DEFAULT NULL COMMENT '用户id',
  `area` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1：审核通过',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `inviteCode` varchar(50) DEFAULT NULL COMMENT '邀请码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopagentuser"
#


#
# Structure for table "shopappversion"
#

DROP TABLE IF EXISTS `shopappversion`;
CREATE TABLE `shopappversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopappversion"
#


#
# Structure for table "shoparea"
#

DROP TABLE IF EXISTS `shoparea`;
CREATE TABLE `shoparea` (
  `id` int(20) DEFAULT NULL,
  `area_name` varchar(150) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `create_dt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shoparea"
#


#
# Structure for table "shopbanner"
#

DROP TABLE IF EXISTS `shopbanner`;
CREATE TABLE `shopbanner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeTitle` varchar(50) DEFAULT NULL COMMENT '跳转类型',
  `Pic` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `Pictures` text COMMENT 'Pictures',
  `Titles` text,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopbanner"
#


#
# Structure for table "shopbbs"
#

DROP TABLE IF EXISTS `shopbbs`;
CREATE TABLE `shopbbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `summary` text,
  `pic` text,
  `userId` int(11) DEFAULT NULL,
  `userPhoto` text,
  `userName` text,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3543 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopbbs"
#


#
# Structure for table "shopcategory"
#

DROP TABLE IF EXISTS `shopcategory`;
CREATE TABLE `shopcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iconImage` varchar(255) DEFAULT NULL COMMENT '图片',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `pid` varchar(11) DEFAULT NULL COMMENT '父id',
  `sortNo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopcategory"
#


#
# Structure for table "shopchanneluser"
#

DROP TABLE IF EXISTS `shopchanneluser`;
CREATE TABLE `shopchanneluser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) DEFAULT NULL COMMENT '用户名',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `inviteCode` varchar(255) DEFAULT NULL COMMENT '邀请码',
  `identCard` varchar(255) DEFAULT NULL COMMENT '身份证',
  `userId` int(11) DEFAULT NULL COMMENT '用户id',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `code` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1：审核通过',
  `createTime` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopchanneluser"
#


#
# Structure for table "shopdelivery"
#

DROP TABLE IF EXISTS `shopdelivery`;
CREATE TABLE `shopdelivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `start` text,
  `end` text,
  `title` varchar(255) DEFAULT NULL,
  `userId` varchar(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `addr` text,
  `type` int(11) DEFAULT NULL,
  `receivedPerson` varchar(255) DEFAULT NULL,
  `receivedPhone` varchar(255) DEFAULT NULL,
  `goodsPic` text,
  `paystate` int(2) DEFAULT '0',
  `orderNo` varchar(500) DEFAULT NULL,
  `toLat` varchar(255) DEFAULT NULL,
  `toLng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='同城帮表';

#
# Data for table "shopdelivery"
#


#
# Structure for table "shopgg"
#

DROP TABLE IF EXISTS `shopgg`;
CREATE TABLE `shopgg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic` varchar(255) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopgg"
#


#
# Structure for table "shopintegral"
#

DROP TABLE IF EXISTS `shopintegral`;
CREATE TABLE `shopintegral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic` varchar(255) DEFAULT NULL COMMENT '图片',
  `title` varchar(255) DEFAULT NULL COMMENT '积分',
  `point` int(11) DEFAULT NULL COMMENT '积分值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='每个人物对应的积分';

#
# Data for table "shopintegral"
#


#
# Structure for table "shopintegralhistory"
#

DROP TABLE IF EXISTS `shopintegralhistory`;
CREATE TABLE `shopintegralhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integralId` int(11) DEFAULT NULL COMMENT '对应积分id',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `userId` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9300 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='积分记录表';

#
# Data for table "shopintegralhistory"
#


#
# Structure for table "shopmoney"
#

DROP TABLE IF EXISTS `shopmoney`;
CREATE TABLE `shopmoney` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createTime` date DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `money` double(11,2) DEFAULT '0.00',
  `coupon` double(11,2) DEFAULT '0.00',
  `charge` double(11,2) DEFAULT '0.00',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23599 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopmoney"
#


#
# Structure for table "shopmoneyhistory"
#

DROP TABLE IF EXISTS `shopmoneyhistory`;
CREATE TABLE `shopmoneyhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `money` double(11,2) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `shopId` int(11) DEFAULT NULL,
  `otherType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=213427 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopmoneyhistory"
#


#
# Structure for table "shoporder"
#

DROP TABLE IF EXISTS `shoporder`;
CREATE TABLE `shoporder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopName` text COMMENT '店铺名称',
  `prize` double(11,2) DEFAULT NULL COMMENT '订单金额',
  `discount` double(11,2) DEFAULT NULL COMMENT '代金券折扣',
  `wxPay` double(11,2) DEFAULT NULL COMMENT '实际支付金额',
  `createTime` datetime DEFAULT NULL COMMENT '下单时间',
  `orderNumber` varchar(100) DEFAULT NULL COMMENT '订单单号',
  `status` int(11) DEFAULT '0' COMMENT '1 未支付 2 支付成功',
  `shopId` int(11) DEFAULT NULL,
  `shopPic` text,
  `userId` int(11) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `name` text,
  `remark` varchar(255) DEFAULT NULL,
  `coupon` double(11,2) DEFAULT NULL,
  `toUserId` int(11) DEFAULT NULL,
  `amount` double(11,2) DEFAULT NULL,
  `addr` text COMMENT '商户实际收款',
  `address` text,
  `isPay` int(2) DEFAULT '0' COMMENT '0 没有支付',
  `pay_type` int(2) DEFAULT NULL COMMENT '1 微信 2支付宝 ',
  `transfer_status` int(2) DEFAULT '0' COMMENT '0 手动转账 1 支付宝自动转账',
  `sendGoods` int(2) DEFAULT '0' COMMENT '0 = 未发货, 1 = 发货',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44331 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shoporder"
#


#
# Structure for table "shopordergoods"
#

DROP TABLE IF EXISTS `shopordergoods`;
CREATE TABLE `shopordergoods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderNo` varchar(100) DEFAULT NULL COMMENT '订单编号',
  `goodsId` int(11) DEFAULT NULL COMMENT '商品ID',
  `prize` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `goodscount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopordergoods"
#


#
# Structure for table "shopproduct"
#

DROP TABLE IF EXISTS `shopproduct`;
CREATE TABLE `shopproduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic` text,
  `price` double(11,2) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT '0' COMMENT '0：用户添加产品 1：兑换商城 2：特卖商品 3：现金+代金券商品 4 甄选',
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `coupon` double(11,2) DEFAULT NULL,
  `norms` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `del` int(1) DEFAULT '0' COMMENT '是否删除(1:已删除；0：有效)',
  `classifyId` int(11) DEFAULT '-1' COMMENT '分类ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13125 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopproduct"
#


#
# Structure for table "shopuser"
#

DROP TABLE IF EXISTS `shopuser`;
CREATE TABLE `shopuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iconImage` text COMMENT '头像',
  `name` varchar(255) DEFAULT NULL COMMENT '昵称',
  `code` bigint(13) DEFAULT NULL COMMENT '用户编号',
  `password` varchar(255) DEFAULT NULL,
  `cash` double(11,2) DEFAULT NULL COMMENT '现金',
  `cash1` double(11,2) DEFAULT NULL COMMENT '服务费',
  `cash2` double(11,2) DEFAULT NULL COMMENT '优惠券',
  `cash3` double(11,2) DEFAULT NULL COMMENT 'K币',
  `isHasBusness` int(255) DEFAULT NULL COMMENT '是否关联门店（1：关联）',
  `Profit` double(11,2) DEFAULT NULL COMMENT '月收益',
  `Order` int(11) DEFAULT NULL COMMENT '本月订单',
  `Order1` int(255) DEFAULT NULL COMMENT '未处理订单',
  `Very` varchar(255) DEFAULT NULL COMMENT '是否认证',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `sex` varchar(255) DEFAULT NULL COMMENT '性别',
  `birthDay` varchar(255) DEFAULT NULL COMMENT '出生日期',
  `address` varchar(255) DEFAULT NULL COMMENT '所在地区',
  `intro` varchar(255) DEFAULT NULL COMMENT '个人简介',
  `industry` varchar(255) DEFAULT NULL COMMENT '从事行业',
  `company` varchar(255) DEFAULT NULL COMMENT '所属单位',
  `duty` varchar(255) DEFAULT NULL COMMENT '职位名称',
  `VeryMsg` varchar(200) DEFAULT NULL,
  `identityId` varchar(200) DEFAULT NULL,
  `inviteCode` varchar(200) DEFAULT NULL COMMENT '邀请码',
  `loginToken` varchar(255) DEFAULT NULL COMMENT '登录token',
  `pubOpenId` varchar(50) DEFAULT NULL COMMENT '公众号OpenId',
  `alipayUid` varchar(255) DEFAULT NULL COMMENT '支付宝id',
  `openId` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '微信APP授权OpenId',
  `dayCash` double(11,2) DEFAULT NULL COMMENT '日收益',
  `os` varchar(255) DEFAULT NULL,
  `deviceId` varchar(255) DEFAULT NULL,
  `isAgent` tinyint(1) DEFAULT NULL COMMENT '0:渠道商；1：代理商',
  `agentType` varchar(255) DEFAULT NULL COMMENT '代理商类型',
  `point` int(11) DEFAULT '0' COMMENT '积分数量',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `aliPayAccount` varchar(40) DEFAULT NULL COMMENT '支付宝账号信息',
  `AliPayName` varchar(40) DEFAULT NULL COMMENT '支付宝用户姓名',
  `hornId` varchar(15) DEFAULT '' COMMENT '喇叭id',
  `unionid` varchar(50) DEFAULT NULL COMMENT '微信用户id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `inx_su_cd` (`code`) USING BTREE,
  KEY `inx_su_pub` (`pubOpenId`) USING BTREE,
  KEY `inx_su_phone` (`phone`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3738289 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "shopuser"
#


#
# Structure for table "shopuseractionlog"
#

DROP TABLE IF EXISTS `shopuseractionlog`;
CREATE TABLE `shopuseractionlog` (
  `id` varchar(36) NOT NULL DEFAULT '' COMMENT 'id',
  `user_id` varchar(36) DEFAULT NULL COMMENT '用户id',
  `data_id` varchar(36) DEFAULT NULL COMMENT '数据id',
  `create_time` datetime NOT NULL COMMENT '操作时间',
  `type` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_user_action` (`user_id`) USING BTREE,
  KEY `idx_user_action_data` (`data_id`) USING BTREE,
  KEY `IDX_USER_ID_TYPE` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户收藏的圈圈或者店铺';

#
# Data for table "shopuseractionlog"
#


#
# Structure for table "station_message"
#

DROP TABLE IF EXISTS `station_message`;
CREATE TABLE `station_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `summary` text COMMENT '内容',
  `createTime` datetime DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='站内信表';

#
# Data for table "station_message"
#


#
# Structure for table "sys_about"
#

DROP TABLE IF EXISTS `sys_about`;
CREATE TABLE `sys_about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "sys_about"
#


#
# Structure for table "sys_change"
#

DROP TABLE IF EXISTS `sys_change`;
CREATE TABLE `sys_change` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `money` double(10,3) NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `balance` double(10,3) NOT NULL,
  `creat_id` bigint(11) NOT NULL,
  `creat_time` datetime NOT NULL,
  `bank1` varchar(50) DEFAULT '',
  `bank2` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='现金转积分';

#
# Data for table "sys_change"
#


#
# Structure for table "sys_currency_history"
#

DROP TABLE IF EXISTS `sys_currency_history`;
CREATE TABLE `sys_currency_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recommend` double(10,3) DEFAULT '0.000' COMMENT '推荐奖',
  `award` double(10,3) DEFAULT NULL COMMENT '报单奖',
  `profit` double(10,3) DEFAULT NULL COMMENT '收益',
  `bonus` double(10,3) DEFAULT '0.000' COMMENT '分红',
  `range` double(10,3) DEFAULT NULL COMMENT '极差奖',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `creat_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` char(1) DEFAULT NULL COMMENT '状态',
  `from_user_name` varchar(50) DEFAULT NULL COMMENT '来源人姓名',
  `from_user_id` bigint(11) DEFAULT NULL COMMENT '来源人id',
  `creat_id` bigint(11) DEFAULT NULL COMMENT '创建人id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3270 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='交易流水';

#
# Data for table "sys_currency_history"
#


#
# Structure for table "sys_dept"
#

DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门管理';

#
# Data for table "sys_dept"
#


#
# Structure for table "sys_dict"
#

DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `sort` decimal(10,0) DEFAULT NULL COMMENT '排序（升序）',
  `parent_id` bigint(64) DEFAULT '0' COMMENT '父级编号',
  `create_by` int(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` bigint(64) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`name`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='字典表';

#
# Data for table "sys_dict"
#


#
# Structure for table "sys_file"
#

DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `save_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7658 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

#
# Data for table "sys_file"
#


#
# Structure for table "sys_file1"
#

DROP TABLE IF EXISTS `sys_file1`;
CREATE TABLE `sys_file1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL COMMENT '文件类型',
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文件上传';

#
# Data for table "sys_file1"
#


#
# Structure for table "sys_grade"
#

DROP TABLE IF EXISTS `sys_grade`;
CREATE TABLE `sys_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_name` varchar(255) DEFAULT NULL COMMENT '等级名称',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `proportion` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "sys_grade"
#

INSERT INTO `sys_grade` VALUES (1,'无',3,0.00),(2,'一星',3,0.10),(3,'二星',10,0.12),(4,'三星',50,0.14),(5,'四星',100,0.16),(6,'五星',300,0.18),(7,'六星',600,0.20),(8,'七星',1000,0.22),(9,'八星',2000,0.24),(10,'九星',5000,0.26);

#
# Structure for table "sys_log"
#

DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `name` varchar(50) DEFAULT '' COMMENT '用户名',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18248 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统日志';

#
# Data for table "sys_log"
#

INSERT INTO `sys_log` VALUES (18248,149,NULL,'houtai','error',NULL,'http://localhost:8000/main','java.lang.NullPointerException',NULL,'2020-10-23 22:58:54'),(18249,149,NULL,'houtai','error',NULL,'http://localhost:8000/main','java.lang.NullPointerException',NULL,'2020-10-23 22:58:54'),(18250,149,'后台','houtai','去会员列表页面',1,'com.pojoin.system.controller.MemberController.partner()',NULL,'127.0.0.1','2020-10-23 22:59:00'),(18251,149,'后台','houtai','全部合伙人列表',29,'com.pojoin.system.controller.MemberController.plist()',NULL,'127.0.0.1','2020-10-23 22:59:00'),(18252,149,'后台','houtai','去会员列表页面',0,'com.pojoin.system.controller.MemberController.mem()',NULL,'127.0.0.1','2020-10-23 22:59:01'),(18253,149,'后台','houtai','全部会员列表',15,'com.pojoin.system.controller.MemberController.memlist()',NULL,'127.0.0.1','2020-10-23 22:59:01'),(18254,149,'后台','houtai','添加用户',1,'com.pojoin.system.controller.MemberController.add()',NULL,'127.0.0.1','2020-10-23 22:59:03'),(18255,149,'后台','houtai','推荐关系图',0,'com.pojoin.system.controller.MemberController.relation()',NULL,'127.0.0.1','2020-10-23 22:59:04'),(18256,149,'后台','houtai','重积分页面',1,'com.pojoin.system.controller.FinanceController.addintegral()',NULL,'127.0.0.1','2020-10-23 22:59:07'),(18257,149,'后台','houtai','充值记录',0,'com.pojoin.system.controller.MemberController.rechargelist()',NULL,'127.0.0.1','2020-10-23 22:59:08'),(18258,149,'后台','houtai','充值记录列表',12,'com.pojoin.system.controller.MemberController.getRechargelist()',NULL,'127.0.0.1','2020-10-23 22:59:08'),(18259,149,NULL,'houtai','error',NULL,'http://localhost:8000/system/tixian','java.lang.NullPointerException',NULL,'2020-10-23 22:59:09'),(18260,149,'后台','houtai','查看收益列表',14,'com.pojoin.system.controller.FinanceController.listCurrent()',NULL,'127.0.0.1','2020-10-23 22:59:11'),(18261,149,'后台','houtai','后台现金转积分记录列表',18,'com.pojoin.system.controller.ChangeController.listHoutai()',NULL,'127.0.0.1','2020-10-23 22:59:12'),(18262,149,'后台','houtai','后台转账列表',13,'com.pojoin.system.controller.TradeController.listHoutai()',NULL,'127.0.0.1','2020-10-23 22:59:13'),(18263,149,'后台','houtai','获取级别',8,'com.pojoin.system.controller.MemberController.findGradeList()',NULL,'127.0.0.1','2020-10-23 22:59:15');

#
# Structure for table "sys_menu"
#

DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单管理';

#
# Data for table "sys_menu"
#

INSERT INTO `sys_menu` VALUES (1,0,'基础管理','','',0,'fa fa-bars',0,'2017-08-09 22:49:47',NULL),(2,3,'系统菜单','sys/menu/','sys:menu:menu',1,'fa fa-th-list',2,'2017-08-09 22:55:15',NULL),(3,0,'系统管理',NULL,NULL,0,'fa fa-desktop',1,'2017-08-09 23:06:55','2017-08-14 14:13:43'),(6,3,'用户管理','sys/user/','sys:user:user',1,'fa fa-user',0,'2017-08-10 14:12:11',NULL),(7,3,'角色管理','sys/role','sys:role:role',1,'fa fa-paw',1,'2017-08-10 14:13:19',NULL),(12,6,'新增','','sys:user:add',2,'',0,'2017-08-14 10:51:35',NULL),(13,6,'编辑','','sys:user:edit',2,'',0,'2017-08-14 10:52:06',NULL),(14,6,'删除',NULL,'sys:user:remove',2,NULL,0,'2017-08-14 10:52:24',NULL),(15,7,'新增','','sys:role:add',2,'',0,'2017-08-14 10:56:37',NULL),(20,2,'新增','','sys:menu:add',2,'',0,'2017-08-14 10:59:32',NULL),(21,2,'编辑','','sys:menu:edit',2,'',0,'2017-08-14 10:59:56',NULL),(22,2,'删除','','sys:menu:remove',2,'',0,'2017-08-14 11:00:26',NULL),(24,6,'批量删除','','sys:user:batchRemove',2,'',0,'2017-08-14 17:27:18',NULL),(25,6,'停用',NULL,'sys:user:disable',2,NULL,0,'2017-08-14 17:27:43',NULL),(26,6,'重置密码','','sys:user:resetPwd',2,'',0,'2017-08-14 17:28:34',NULL),(27,91,'系统日志','common/log','common:log',1,'fa fa-warning',0,'2017-08-14 22:11:53',NULL),(28,27,'刷新',NULL,'sys:log:list',2,NULL,0,'2017-08-14 22:30:22',NULL),(29,27,'删除',NULL,'sys:log:remove',2,NULL,0,'2017-08-14 22:30:43',NULL),(30,27,'清空',NULL,'sys:log:clear',2,NULL,0,'2017-08-14 22:31:02',NULL),(48,77,'代码生成','common/generator','common:generator',1,'fa fa-code',3,NULL,NULL),(49,0,'信息管理','','',0,'fa fa-rss',6,NULL,NULL),(50,49,'信息列表','blog/bContent','blog:bContent:bContent',1,'fa fa-file-image-o',1,NULL,NULL),(51,50,'新增','','blog:bContent:add',2,'',NULL,NULL,NULL),(55,7,'编辑','','sys:role:edit',2,'',NULL,NULL,NULL),(56,7,'删除','','sys:role:remove',2,NULL,NULL,NULL,NULL),(57,91,'运行监控','/druid/index.html','',1,'fa fa-caret-square-o-right',1,NULL,NULL),(58,50,'编辑','','blog:bContent:edit',2,NULL,NULL,NULL,NULL),(59,50,'删除','','blog:bContent:remove',2,NULL,NULL,NULL,NULL),(60,50,'批量删除','','blog:bContent:batchRemove',2,NULL,NULL,NULL,NULL),(61,2,'批量删除','','sys:menu:batchRemove',2,NULL,NULL,NULL,NULL),(62,7,'批量删除','','sys:role:batchRemove',2,NULL,NULL,NULL,NULL),(68,49,'发布文章','/blog/bContent/add','blog:bContent:add',1,'fa fa-edit',0,NULL,NULL),(71,1,'文件管理','/common/sysFile','common:sysFile:sysFile',1,'fa fa-folder-open',2,NULL,NULL),(72,77,'计划任务','common/job','common:taskScheduleJob',1,'fa fa-hourglass-1',4,NULL,NULL),(73,3,'部门管理','/system/sysDept','system:sysDept:sysDept',1,'fa fa-users',3,NULL,NULL),(74,73,'增加','/system/sysDept/add','system:sysDept:add',2,NULL,1,NULL,NULL),(75,73,'刪除','system/sysDept/remove','system:sysDept:remove',2,NULL,2,NULL,NULL),(76,73,'编辑','/system/sysDept/edit','system:sysDept:edit',2,NULL,3,NULL,NULL),(77,0,'系统工具','','',0,'fa fa-gear',4,NULL,NULL),(78,1,'数据字典','/common/dict','common:dict:dict',1,'fa fa-book',1,NULL,NULL),(79,78,'增加','/common/dict/add','common:dict:add',2,NULL,2,NULL,NULL),(80,78,'编辑','/common/dict/edit','common:dict:edit',2,NULL,2,NULL,NULL),(81,78,'删除','/common/dict/remove','common:dict:remove',2,'',3,NULL,NULL),(83,78,'批量删除','/common/dict/batchRemove','common:dict:batchRemove',2,'',4,NULL,NULL),(91,0,'系统监控','','',0,'fa fa-video-camera',5,NULL,NULL),(92,91,'在线用户','sys/online','',1,'fa fa-user',NULL,NULL,NULL),(97,0,'图表管理','','',0,'fa fa-bar-chart',7,NULL,NULL),(98,97,'百度chart','/chart/graph_echarts.html','',1,'fa fa-area-chart',NULL,NULL,NULL),(104,77,'swagger','/swagger-ui.html','',1,'',NULL,NULL,NULL),(105,0,'财务中心','','',0,'fa fa-bar-chart-o',6,NULL,NULL),(106,105,'充值','/sys/fin/add','',1,'fa fa-area-chart',1,NULL,NULL),(107,105,'提现','/system/tixian/add','',1,'fa fa-bar-chart-o',2,NULL,NULL),(108,105,'收入明细','/sys/fin','',1,'fa fa-bar-chart',5,NULL,NULL),(109,0,'会员管理','','',0,'fa fa-user-o',5,NULL,NULL),(110,109,'会员注册','/sys/mem/add','',1,'fa fa-building-o',1,NULL,NULL),(111,109,'我的推荐','/sys/mem','',1,'fa fa-address-book-o',NULL,NULL,NULL),(112,109,'推荐关系图','/sys/mem/relation','',1,'fa fa-user',3,NULL,NULL),(113,0,'会员信息管理','','',0,'fa fa-address-card',1,NULL,NULL),(114,113,'会员列表','/sys/mem/mem1','',1,'fa fa-address-book-o',NULL,NULL,NULL),(115,0,'财务信息管理','','',0,'fa fa-bar-chart',2,NULL,NULL),(116,115,'提款记录','/system/tixian','',1,'fa fa-bar-chart',3,NULL,NULL),(117,115,'充值记录','/sys/mem/rechargelist','',1,'fa fa-asl-interpreting',2,NULL,NULL),(118,0,'市场信息管理','','',0,'fa fa-bar-chart',3,NULL,NULL),(119,118,'推荐结构图','/sys/mem/relation','',1,'fa fa-address-card',2,NULL,NULL),(120,118,'报单中心列表','/sys/mem/orderlist','',1,'fa fa-address-card-o',1,NULL,NULL),(121,0,'系统设置','','',0,'fa fa-address-book',4,NULL,NULL),(122,121,'比例设置','/sys/mem/findGradeList','',1,'fa fa-american-sign-language-interpreting',1,NULL,NULL),(123,115,'手动充值','/sys/fin/addintegral','',1,'fa fa-battery-4',2,NULL,NULL),(124,105,'提现记录','/system/tixian','',1,'fa fa-line-chart',4,NULL,NULL),(125,105,'充值列表','/system/order','',1,'fa fa-bar-chart-o',3,NULL,NULL),(126,105,'现金转积分','/system/change','system:change:change',1,'fa fa-bar-chart',7,NULL,NULL),(127,105,'出账列表','/system/trade','system:trade:trade',1,'fa fa-bar-chart-o',8,NULL,NULL),(128,105,'入账列表','/system/trade/trade1','system:trade:trade',1,'fa fa-bar-chart',9,NULL,NULL),(129,126,'转换','/system/change/add','system:change:add',2,'',1,NULL,NULL),(130,127,'转赠','/system/trade/add','system:trade:add',2,'',1,NULL,NULL),(131,115,'会员收入明细','/sys/fin','',1,'',4,NULL,NULL),(132,115,'现金转换记录','/system/change/changeHoutai','system:change:change',1,'',5,NULL,NULL),(133,115,'转账记录','/system/trade/getHoutai','system:trade:trade',1,'',6,NULL,NULL),(134,113,'会员注册','/sys/mem/add','',1,'',3,NULL,NULL),(135,113,'推荐关系图','/sys/mem/relation','',1,'',3,NULL,NULL),(136,113,'合伙人列表','/sys/mem/partner1','',1,'fa fa-address-book-o',NULL,NULL,NULL);

#
# Structure for table "sys_money_history"
#

DROP TABLE IF EXISTS `sys_money_history`;
CREATE TABLE `sys_money_history` (
  `id` int(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(19) DEFAULT '0' COMMENT '用户id',
  `money` double(10,2) DEFAULT '0.00' COMMENT '现金',
  `balance` double(10,2) DEFAULT '0.00' COMMENT '积分',
  `type` char(1) DEFAULT NULL COMMENT 'm:钱；b：积分',
  `creat_time` datetime DEFAULT NULL COMMENT '创建时间',
  `bank1` varchar(50) DEFAULT NULL,
  `bank2` varchar(50) DEFAULT NULL,
  `from_user_id` bigint(19) DEFAULT NULL COMMENT '来源',
  `remark` varchar(50) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1511 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='资金流水记录';

#
# Data for table "sys_money_history"
#


#
# Structure for table "sys_order"
#

DROP TABLE IF EXISTS `sys_order`;
CREATE TABLE `sys_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(50) DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '付款人\r\n付款用户id',
  `status` char(1) DEFAULT NULL COMMENT '支付状态0未拨币1已拨币；2:已撤销',
  `source` char(255) DEFAULT NULL COMMENT '来源',
  `from_username` varchar(255) DEFAULT NULL COMMENT '付款人姓名',
  `to_username` varchar(255) DEFAULT NULL COMMENT '收款人姓名',
  `creat_time` datetime DEFAULT NULL COMMENT '创建时间',
  `from_account` varchar(255) DEFAULT NULL COMMENT '付款人账号',
  `to_account` varchar(255) DEFAULT NULL COMMENT '收款人账号',
  `money` double(10,2) DEFAULT NULL COMMENT '付款金额',
  `pay_type` varchar(20) DEFAULT NULL COMMENT '支付方式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=539 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "sys_order"
#


#
# Structure for table "sys_role"
#

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `role_sign` varchar(100) DEFAULT NULL COMMENT '角色标识',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色';

#
# Data for table "sys_role"
#

INSERT INTO `sys_role` VALUES (1,'超级用户角色','admin','拥有最高权限',2,'2017-08-12 00:43:52','2017-08-12 19:14:59'),(59,'普通用户',NULL,'基本用户权限',NULL,NULL,NULL),(61,'系统管理员',NULL,'总后台账号',NULL,NULL,NULL),(62,'会员',NULL,'报单中心',NULL,NULL,NULL),(63,'商家',NULL,'店铺',NULL,NULL,NULL),(64,'售后',NULL,'售后账号',NULL,NULL,NULL);

#
# Structure for table "sys_role_menu"
#

DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4181 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色与菜单对应关系';

#
# Data for table "sys_role_menu"
#

INSERT INTO `sys_role_menu` VALUES (367,44,1),(368,44,32),(369,44,33),(370,44,34),(371,44,35),(372,44,28),(373,44,29),(374,44,30),(375,44,38),(376,44,4),(377,44,27),(378,45,38),(379,46,3),(380,46,20),(381,46,21),(382,46,22),(383,46,23),(384,46,11),(385,46,12),(386,46,13),(387,46,14),(388,46,24),(389,46,25),(390,46,26),(391,46,15),(392,46,2),(393,46,6),(394,46,7),(598,50,38),(632,38,42),(737,51,38),(738,51,39),(739,51,40),(740,51,41),(741,51,4),(742,51,32),(743,51,33),(744,51,34),(745,51,35),(746,51,27),(747,51,28),(748,51,29),(749,51,30),(750,51,1),(1064,54,53),(1095,55,2),(1096,55,6),(1097,55,7),(1098,55,3),(1099,55,50),(1100,55,49),(1101,55,1),(1856,53,28),(1857,53,29),(1858,53,30),(1859,53,27),(1860,53,57),(1861,53,71),(1862,53,48),(1863,53,72),(1864,53,1),(1865,53,7),(1866,53,55),(1867,53,56),(1868,53,62),(1869,53,15),(1870,53,2),(1871,53,61),(1872,53,20),(1873,53,21),(1874,53,22),(2084,56,68),(2085,56,60),(2086,56,59),(2087,56,58),(2088,56,51),(2089,56,50),(2090,56,49),(2243,48,72),(2258,65,89),(2259,65,88),(2260,65,86),(2262,67,48),(2263,68,88),(2264,68,87),(2265,69,89),(2266,69,88),(2267,69,86),(2268,69,87),(2269,69,85),(2270,69,84),(2271,70,85),(2272,70,89),(2273,70,88),(2274,70,87),(2275,70,86),(2276,70,84),(2277,71,87),(2278,72,59),(2279,73,48),(2280,74,88),(2281,74,87),(2282,75,88),(2283,75,87),(2284,76,85),(2285,76,89),(2286,76,88),(2287,76,87),(2288,76,86),(2289,76,84),(2292,78,88),(2293,78,87),(2294,78,NULL),(2295,78,NULL),(2296,78,NULL),(2308,80,87),(2309,80,86),(2310,80,-1),(2311,80,84),(2312,80,85),(2328,79,72),(2329,79,48),(2330,79,77),(2331,79,84),(2332,79,89),(2333,79,88),(2334,79,87),(2335,79,86),(2336,79,85),(2337,79,-1),(2338,77,89),(2339,77,88),(2340,77,87),(2341,77,86),(2342,77,85),(2343,77,84),(2344,77,72),(2345,77,-1),(2346,77,77),(2974,57,93),(2975,57,99),(2976,57,95),(2977,57,101),(2978,57,96),(2979,57,94),(2980,57,-1),(2981,58,93),(2982,58,99),(2983,58,95),(2984,58,101),(2985,58,96),(2986,58,94),(2987,58,-1),(3232,59,98),(3233,59,101),(3234,59,99),(3235,59,95),(3236,59,90),(3237,59,89),(3238,59,88),(3239,59,87),(3240,59,86),(3241,59,68),(3242,59,60),(3243,59,59),(3244,59,58),(3245,59,51),(3246,59,76),(3247,59,75),(3248,59,74),(3249,59,62),(3250,59,56),(3251,59,55),(3252,59,15),(3253,59,26),(3254,59,25),(3255,59,24),(3256,59,14),(3257,59,13),(3258,59,12),(3259,59,61),(3260,59,22),(3261,59,21),(3262,59,20),(3263,59,83),(3264,59,81),(3265,59,80),(3266,59,79),(3267,59,71),(3268,59,97),(3269,59,96),(3270,59,94),(3271,59,93),(3272,59,85),(3273,59,84),(3274,59,50),(3275,59,49),(3276,59,73),(3277,59,7),(3278,59,6),(3279,59,2),(3280,59,3),(3281,59,78),(3282,59,1),(3283,59,-1),(4016,62,111),(4017,62,110),(4018,62,128),(4019,62,129),(4020,62,125),(4021,62,124),(4022,62,108),(4023,62,107),(4024,62,106),(4025,62,126),(4026,62,127),(4027,62,130),(4028,62,105),(4029,62,-1),(4030,62,109),(4049,1,122),(4050,1,120),(4051,1,119),(4052,1,123),(4053,1,117),(4054,1,116),(4055,1,114),(4056,1,112),(4057,1,111),(4058,1,110),(4059,1,125),(4060,1,124),(4061,1,108),(4062,1,107),(4063,1,106),(4064,1,92),(4065,1,57),(4066,1,30),(4067,1,29),(4068,1,28),(4069,1,76),(4070,1,75),(4071,1,74),(4072,1,62),(4073,1,56),(4074,1,55),(4075,1,15),(4076,1,26),(4077,1,25),(4078,1,24),(4079,1,14),(4080,1,13),(4081,1,12),(4082,1,61),(4083,1,22),(4084,1,21),(4085,1,20),(4086,1,121),(4087,1,118),(4088,1,115),(4089,1,113),(4090,1,109),(4091,1,27),(4092,1,91),(4093,1,73),(4094,1,7),(4095,1,6),(4096,1,2),(4097,1,3),(4098,1,50),(4099,1,60),(4100,1,59),(4101,1,58),(4102,1,51),(4103,1,-1),(4104,1,105),(4105,1,49),(4137,61,122),(4138,61,133),(4139,61,132),(4140,61,131),(4141,61,123),(4142,61,117),(4143,61,116),(4144,61,114),(4145,61,121),(4146,61,115),(4147,61,135),(4148,61,134),(4149,61,113),(4150,61,-1),(4151,63,111),(4152,63,130),(4153,63,125),(4154,63,124),(4155,63,108),(4156,63,107),(4157,63,127),(4158,63,128),(4159,63,-1),(4160,63,109),(4161,63,105),(4168,64,134),(4169,64,133),(4170,64,132),(4171,64,131),(4172,64,117),(4173,64,116),(4174,64,114),(4175,64,135),(4176,64,113),(4177,64,-1),(4178,64,115),(4179,1,136),(4180,61,136);

#
# Structure for table "sys_task"
#

DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cron_expression` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `method_name` varchar(255) DEFAULT NULL COMMENT '任务调用的方法名',
  `is_concurrent` varchar(255) DEFAULT NULL COMMENT '任务是否有状态',
  `description` varchar(255) DEFAULT NULL COMMENT '任务描述',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `bean_class` varchar(255) DEFAULT NULL COMMENT '任务执行时调用哪个类的方法 包名+类名',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `job_status` varchar(255) DEFAULT NULL COMMENT '任务状态',
  `job_group` varchar(255) DEFAULT NULL COMMENT '任务分组',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `spring_bean` varchar(255) DEFAULT NULL COMMENT 'Spring bean',
  `job_name` varchar(255) DEFAULT NULL COMMENT '任务名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "sys_task"
#

/*!40000 ALTER TABLE `sys_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_task` ENABLE KEYS */;

#
# Structure for table "sys_tixian"
#

DROP TABLE IF EXISTS `sys_tixian`;
CREATE TABLE `sys_tixian` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `bank` varchar(255) DEFAULT NULL COMMENT '开户行平台',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `accout` varchar(255) DEFAULT NULL COMMENT '开户行账号',
  `phone` varchar(255) DEFAULT NULL COMMENT '提款人电话',
  `money` varchar(255) DEFAULT NULL COMMENT '提款金额',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `bank1` varchar(255) DEFAULT NULL COMMENT '备用字段',
  `bank2` varchar(255) DEFAULT NULL COMMENT '备用字段',
  `service_money` varchar(255) DEFAULT NULL COMMENT '手续费',
  `balance` varchar(255) DEFAULT NULL COMMENT '余额',
  `status` varchar(255) DEFAULT NULL COMMENT '状态0：未支付；1：已支付',
  `creat_time` datetime DEFAULT NULL COMMENT '创建时间',
  `pay_time` datetime DEFAULT NULL COMMENT '提款时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='提现表';

#
# Data for table "sys_tixian"
#


#
# Structure for table "sys_trade"
#

DROP TABLE IF EXISTS `sys_trade`;
CREATE TABLE `sys_trade` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) DEFAULT NULL,
  `touser_id` bigint(11) DEFAULT NULL,
  `money` double(10,3) DEFAULT NULL,
  `creat_id` bigint(11) DEFAULT NULL,
  `creat_time` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL COMMENT '1:转入；2：转出',
  `bank1` varchar(50) DEFAULT NULL,
  `bank2` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='现金转增交易';

#
# Data for table "sys_trade"
#


#
# Structure for table "sys_user"
#

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `balance` double(10,2) DEFAULT '0.00' COMMENT '报单积分',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `grade` varchar(255) DEFAULT NULL COMMENT '等级',
  `code` varchar(30) DEFAULT NULL COMMENT '会员编号',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级id',
  `money` double(10,3) DEFAULT '0.000' COMMENT '余额：收益',
  `parent_link` text,
  `invitate_code` varchar(255) DEFAULT NULL COMMENT '推荐人编号',
  `core_code` varchar(255) DEFAULT NULL COMMENT '报单中心编号',
  `idcard` varchar(255) DEFAULT NULL COMMENT '证件编号：身份证',
  `level` varchar(255) DEFAULT NULL COMMENT '级别',
  `dept_id` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 0:禁用，1:正常',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `sex` bigint(32) DEFAULT NULL COMMENT '性别',
  `birth` datetime DEFAULT NULL COMMENT '出身日期',
  `pic_id` bigint(32) DEFAULT NULL,
  `live_address` varchar(500) DEFAULT NULL COMMENT '现居住地',
  `hobby` varchar(255) DEFAULT NULL COMMENT '年龄',
  `province` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `district` varchar(255) DEFAULT NULL COMMENT '所在地区',
  `ali_pay_account` varchar(100) DEFAULT NULL,
  `ali_pay_name` varchar(100) DEFAULT NULL,
  `option` double(10,2) DEFAULT '0.00' COMMENT '期权',
  `partner` int(11) DEFAULT NULL COMMENT '0: 普通 1：合伙人 2：董事 ',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42917 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户管理';

#
# Data for table "sys_user"
#

INSERT INTO `sys_user` VALUES (1,'超级管理员','hhlm',0.00,'17699999999','0','',1,0.000,'','1','1','12','0',6,'admin@example.com',2,1,'2577aa5ca13e423752199f3f15e66fea','2017-08-15 21:40:39','2017-08-15 21:41:00',96,'2017-12-14 00:00:00',148,'ccc','122;121;','河北省','石家庄市','长安区',NULL,NULL,0.00,NULL),(2,'临时用户','test',0.00,NULL,'0',NULL,0,0.000,'',NULL,NULL,NULL,'0',6,'test@bootdo.com',1,1,'6cf3bb3deba2aadbd41ec9a22511084e','2017-08-14 13:43:05','2017-08-14 21:15:36',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL),(147,'业务员','业务员',0.00,NULL,'0','',0,0.000,'',NULL,NULL,NULL,'0',6,'1334657540@qq.com',1,NULL,'da95a2e0774dc487fd1b2adf0caafbe2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL),(148,'惠会联盟售后','shouhou',0.00,'','无','',0,0.000,'0',NULL,NULL,'','',6,'1334657540@qq.com',1,NULL,'4ba5e5ee3d470cd3b36fab06c8f9ce22',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL),(149,'后台','houtai',135679.00,'','五星','1',0,48016.175,'0',NULL,NULL,'','2',10,'1334657540@qq.com',1,NULL,'2577aa5ca13e423752199f3f15e66fea',NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL),(25367,'衡水惠而会网络科技有限公司','12345',1000000.00,'','五星','12345',149,1903.137,'0,149','1','1','','1',40,'',1,NULL,'f9844d2c3675cde206af3a49873757d1',NULL,NULL,0,NULL,NULL,'','',NULL,NULL,'',NULL,'2764.160',0.00,NULL);

#
# Structure for table "sys_user_copy_0807"
#

DROP TABLE IF EXISTS `sys_user_copy_0807`;
CREATE TABLE `sys_user_copy_0807` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `balance` double(10,2) DEFAULT '0.00' COMMENT '报单积分',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `grade` varchar(255) DEFAULT NULL COMMENT '等级',
  `code` varchar(30) DEFAULT NULL COMMENT '会员编号',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级id',
  `money` double(10,3) DEFAULT '0.000' COMMENT '余额：收益',
  `parent_link` text,
  `invitate_code` varchar(255) DEFAULT NULL COMMENT '推荐人编号',
  `core_code` varchar(255) DEFAULT NULL COMMENT '报单中心编号',
  `idcard` varchar(255) DEFAULT NULL COMMENT '证件编号：身份证',
  `level` varchar(255) DEFAULT NULL COMMENT '级别',
  `dept_id` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 0:禁用，1:正常',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `sex` bigint(32) DEFAULT NULL COMMENT '性别',
  `birth` datetime DEFAULT NULL COMMENT '出身日期',
  `pic_id` bigint(32) DEFAULT NULL,
  `live_address` varchar(500) DEFAULT NULL COMMENT '现居住地',
  `hobby` varchar(255) DEFAULT NULL COMMENT '年龄',
  `province` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `district` varchar(255) DEFAULT NULL COMMENT '所在地区',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=40729 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户管理';

#
# Data for table "sys_user_copy_0807"
#


#
# Structure for table "sys_user_copy1"
#

DROP TABLE IF EXISTS `sys_user_copy1`;
CREATE TABLE `sys_user_copy1` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '真实姓名',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `balance` double(10,2) DEFAULT '0.00' COMMENT '报单积分',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `grade` varchar(255) DEFAULT NULL COMMENT '等级',
  `code` varchar(30) DEFAULT NULL COMMENT '会员编号',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级id',
  `money` double(10,3) DEFAULT '0.000' COMMENT '余额：收益',
  `parent_link` text,
  `invitate_code` varchar(255) DEFAULT NULL COMMENT '推荐人编号',
  `core_code` varchar(255) DEFAULT NULL COMMENT '报单中心编号',
  `idcard` varchar(255) DEFAULT NULL COMMENT '证件编号：身份证',
  `level` varchar(255) DEFAULT NULL COMMENT '级别',
  `dept_id` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 0:禁用，1:正常',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `sex` bigint(32) DEFAULT NULL COMMENT '性别',
  `birth` datetime DEFAULT NULL COMMENT '出身日期',
  `pic_id` bigint(32) DEFAULT NULL,
  `live_address` varchar(500) DEFAULT NULL COMMENT '现居住地',
  `hobby` varchar(255) DEFAULT NULL COMMENT '年龄',
  `province` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `district` varchar(255) DEFAULT NULL COMMENT '所在地区',
  `ali_pay_account` varchar(100) DEFAULT NULL,
  `ali_pay_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42609 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户管理';

#
# Data for table "sys_user_copy1"
#


#
# Structure for table "sys_user_plus"
#

DROP TABLE IF EXISTS `sys_user_plus`;
CREATE TABLE `sys_user_plus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `payment` double DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "sys_user_plus"
#


#
# Structure for table "sys_user_role"
#

DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=663 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户与角色对应关系';

#
# Data for table "sys_user_role"
#

INSERT INTO `sys_user_role` VALUES (73,30,48),(74,30,49),(75,30,50),(76,31,48),(77,31,49),(78,31,52),(79,32,48),(80,32,49),(81,32,50),(82,32,51),(83,32,52),(84,33,38),(85,33,49),(86,33,52),(87,34,50),(88,34,51),(89,34,52),(106,124,1),(110,1,1),(111,2,1),(113,131,48),(117,135,1),(120,134,1),(121,134,48),(123,130,1),(124,NULL,48),(125,132,52),(126,132,49),(127,123,48),(132,36,48),(141,138,59),(142,143,1),(143,144,59),(145,145,1),(147,147,61),(149,149,61),(312,319,62),(313,320,62),(314,321,62),(315,322,62),(316,323,62),(317,324,62),(318,325,62),(319,326,62),(320,327,63),(321,328,62),(322,329,63),(323,330,63),(324,737,62),(325,748,62),(326,759,62),(327,1377,62),(328,1387,62),(329,1397,62),(330,1407,62),(331,2623,63),(332,2624,63),(333,2625,63),(334,2626,63),(335,2627,63),(336,2628,63),(337,2629,63),(338,2630,62),(339,2640,62),(340,2650,62),(341,2667,63),(342,2668,62),(343,2678,63),(344,2679,63),(345,2680,63),(346,2681,62),(347,2691,63),(348,2692,63),(349,2693,63),(350,2694,62),(351,2704,62),(352,2714,62),(353,2725,62),(354,2735,62),(355,2745,62),(356,2755,62),(357,2765,62),(358,2775,63),(359,2776,63),(360,2777,63),(361,2778,63),(362,2779,62),(363,2789,62),(364,2799,62),(365,2809,62),(366,2819,63),(367,2820,62),(368,4036,63),(369,4037,63),(370,4038,62),(371,5254,63),(372,5255,62),(373,5265,62),(374,5275,62),(375,5285,62),(376,5295,62),(377,5305,62),(378,5315,63),(379,5316,62),(380,6532,63),(381,6533,62),(382,6543,62),(383,6553,62),(384,6653,62),(385,6753,62),(387,6763,62),(388,6764,62),(389,7980,62),(390,7990,62),(391,8000,62),(392,9216,62),(393,9226,62),(394,9236,63),(395,9237,62),(396,9247,62),(397,10463,62),(398,10473,62),(399,11085,62),(400,12301,62),(401,12311,63),(402,12312,63),(403,12313,63),(404,12314,63),(405,12315,62),(406,13531,62),(407,13541,62),(408,13551,63),(409,13552,62),(410,14768,62),(411,14778,63),(412,14779,62),(413,15995,62),(414,16005,63),(415,16006,63),(416,16007,63),(417,16008,63),(418,16009,63),(419,16010,62),(420,16622,62),(421,16632,63),(422,16633,63),(423,16634,63),(424,16635,63),(425,16636,62),(426,17248,62),(427,17258,62),(428,17268,62),(429,17278,63),(430,17279,63),(431,17280,62),(432,18496,62),(433,18506,63),(434,18507,62),(435,18517,63),(436,18518,63),(437,18519,63),(438,18520,62),(439,18530,62),(440,19746,63),(441,19747,63),(442,19748,62),(443,19749,62),(444,20361,63),(445,20362,63),(446,20363,63),(447,20364,63),(448,20365,63),(449,20366,62),(450,21582,62),(451,21592,63),(452,21593,62),(453,22205,62),(455,22216,62),(456,23522,62),(457,22215,62),(458,23532,63),(459,23533,63),(460,23534,63),(461,23535,63),(462,23536,63),(463,23537,63),(465,23538,62),(466,24141,62),(467,25357,62),(468,25367,62),(469,25979,62),(470,27285,62),(471,28501,62),(472,28502,62),(474,28702,62),(475,28802,62),(477,29902,62),(483,29934,62),(484,29912,62),(485,29913,62),(489,30353,62),(490,30404,62),(491,31710,63),(492,32917,63),(494,38345,62),(495,38355,62),(496,38365,63),(498,38344,62),(501,38387,62),(502,38398,63),(503,38449,63),(504,38450,63),(505,38460,63),(508,38461,62),(509,38462,62),(510,39085,62),(511,39095,63),(513,39096,62),(514,29933,62),(515,39146,63),(516,39147,63),(517,39148,63),(520,39150,62),(521,39154,63),(522,39155,63),(523,39149,62),(525,39166,62),(526,39177,63),(529,29923,62),(531,39181,63),(532,39180,62),(533,39182,63),(534,39183,63),(535,39184,63),(536,39179,62),(537,39195,63),(543,39201,63),(544,39202,62),(547,38366,62),(549,39275,62),(550,39178,62),(551,39296,63),(553,39297,62),(554,39398,63),(555,39399,63),(557,39400,62),(558,39510,63),(559,39511,63),(562,39513,62),(563,39512,62),(564,40728,63),(565,40729,63),(574,40738,63),(575,40739,63),(576,40740,63),(577,40741,62),(578,40841,63),(579,40842,63),(580,40843,63),(581,40844,62),(582,148,64),(583,40854,63),(584,40855,63),(585,40856,62),(586,41256,63),(587,41257,63),(588,41258,63),(589,41259,63),(590,41260,63),(594,41281,62),(595,41271,62),(596,41261,62),(598,42164,63),(599,42165,63),(603,42276,62),(604,42386,63),(606,42387,62),(607,42166,62),(608,42497,63),(610,42498,62),(611,42608,63),(612,42609,63),(613,42610,63),(615,42611,62),(616,42721,63),(617,42163,62),(618,42732,63),(619,42733,63),(620,42734,63),(621,28602,62),(622,42735,63),(623,42736,63),(624,42737,63),(625,42738,63),(627,42739,62),(628,42849,63),(630,42850,62),(631,42860,63),(633,42861,63),(635,42872,62),(636,42882,63),(637,42883,63),(638,42884,63),(639,42885,63),(640,42886,63),(641,42887,63),(642,42888,63),(643,42889,63),(644,42890,63),(645,42891,63),(646,42892,63),(647,42893,63),(648,42894,63),(650,42895,62),(651,42896,63),(652,42906,63),(653,42907,63),(654,42908,63),(655,42909,63),(656,42910,63),(657,42911,63),(658,42912,63),(659,42913,63),(660,42914,63),(661,42915,63),(662,42916,63);

#
# Structure for table "t_draw_reward"
#

DROP TABLE IF EXISTS `t_draw_reward`;
CREATE TABLE `t_draw_reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `draws_name` varchar(255) DEFAULT NULL COMMENT '奖品名称',
  `reward_id` varchar(255) DEFAULT NULL COMMENT '奖项id',
  `days_effect` varchar(255) DEFAULT NULL COMMENT '有效天数',
  `scope` varchar(255) DEFAULT NULL COMMENT '适用范围',
  `pic` varchar(255) DEFAULT NULL COMMENT '奖品图片地址',
  `daily_ceiling` varchar(255) DEFAULT NULL COMMENT '每日上限',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `activity_rule` varchar(255) DEFAULT NULL COMMENT '活动规则',
  `draw_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "t_draw_reward"
#


#
# Structure for table "t_draws"
#

DROP TABLE IF EXISTS `t_draws`;
CREATE TABLE `t_draws` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `phone` varchar(255) DEFAULT NULL COMMENT '中奖手机号',
  `status` varchar(2) DEFAULT '1' COMMENT '0必填/1非',
  `draws_number` varchar(255) DEFAULT NULL COMMENT '抽奖次数',
  `max_draws` varchar(255) DEFAULT NULL COMMENT '最多中将次数',
  `draws_name` varchar(255) DEFAULT NULL COMMENT '奖品名称',
  `code` int(255) DEFAULT NULL COMMENT '商户code',
  `user_id` int(11) DEFAULT NULL COMMENT '商家的userid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "t_draws"
#


#
# Structure for table "t_reward"
#

DROP TABLE IF EXISTS `t_reward`;
CREATE TABLE `t_reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reward` varchar(255) DEFAULT NULL COMMENT '奖项',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "t_reward"
#


#
# Structure for table "user_address"
#

DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `id` int(19) NOT NULL,
  `user_id` int(19) NOT NULL DEFAULT '0' COMMENT '用户id',
  `province_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(11) NOT NULL DEFAULT '0',
  `county_id` int(11) NOT NULL DEFAULT '0',
  `addres` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '详细地址',
  `is_default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:非默认；1：默认地址',
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `bank1` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `bank2` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户地址表';

#
# Data for table "user_address"
#


#
# Structure for table "user_qrcode"
#

DROP TABLE IF EXISTS `user_qrcode`;
CREATE TABLE `user_qrcode` (
  `id` int(19) NOT NULL AUTO_INCREMENT COMMENT '注解',
  `userId` int(19) DEFAULT NULL COMMENT '用户id',
  `qrcode` varchar(20) DEFAULT NULL COMMENT '商户码',
  `creatTime` datetime DEFAULT NULL COMMENT '创建时间',
  `status` char(1) DEFAULT NULL COMMENT '状态（1：有效；0：失效）',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `qrcode` (`qrcode`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户二维码';

#
# Data for table "user_qrcode"
#


#
# Structure for table "经纬度"
#

DROP TABLE IF EXISTS `经纬度`;
CREATE TABLE `经纬度` (
  `government` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `N` varchar(255) DEFAULT NULL,
  `E` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

#
# Data for table "经纬度"
#


#
# Structure for table "应用表"
#

DROP TABLE IF EXISTS `应用表`;
CREATE TABLE `应用表` (
  `category` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

#
# Data for table "应用表"
#
INSERT INTO `hhlm`.`sys_grade` (`id`, `grade_name`, `number`, `proportion`) VALUES ('11', '一代', '300', '0.35');
INSERT INTO `hhlm`.`sys_grade` (`id`, `grade_name`, `number`, `proportion`) VALUES ('12', '二代', '300', '0.35');
ALTER TABLE `hhlm`.`sys_user`
ADD COLUMN `num` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '邀请人数' AFTER `partner`;
UPDATE `hhlm`.`sys_grade` SET `number` = '0' WHERE (`id` = '1');


