package com.pojoin.system.controller;

import java.util.List;
import java.util.Map;

import com.pojoin.common.annotation.Log;
import com.pojoin.common.controller.BaseController;
import com.pojoin.system.domain.UserDO;
import com.pojoin.system.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pojoin.system.domain.ChangeDO;
import com.pojoin.system.service.ChangeService;
import com.pojoin.common.utils.PageUtils;
import com.pojoin.common.utils.Query;
import com.pojoin.common.utils.R;

/**
 * 现金转积分
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-13 10:33:00
 */
 
@Controller
@RequestMapping("/system/change")
public class ChangeController extends BaseController {
	@Autowired
	private ChangeService changeService;
	@Autowired
	private UserService userService;


	@GetMapping()
	@RequiresPermissions("system:change:change")
	String Change(){
	    return "system/change/change";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("system:change:change")
	@Log("现金转积分列表")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
		params.put("userId",getUserId());
        Query query = new Query(params);
		List<ChangeDO> changeList = changeService.list(query);
		int total = changeService.count(query);
		PageUtils pageUtils = new PageUtils(changeList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("system:change:add")
	String add(Model model){
		Double money=userService.get(getUserId()).getMoney();
		model.addAttribute("cash",money);
	    return "system/change/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("system:change:edit")
	String edit(@PathVariable("id") Long id,Model model){
		ChangeDO change = changeService.get(id);
		model.addAttribute("change", change);
	    return "system/change/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("system:change:add")
	@Log("保存现金转积分记录")
	public R save( ChangeDO change){
		UserDO userDO= userService.get(getUserId());
		if(userDO==null){
			return R.error();
		}
		Double money = userDO.getMoney();
		change.setBank1(userDO.getUsername());
		if(money<change.getMoney()){
			return R.error("余额不足");
		}
		change.setCreatId(getUserId());
		change.setUserId(getUserId());
		if(changeService.save(change)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("system:change:edit")
	public R update( ChangeDO change){
		changeService.update(change);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("system:change:remove")
	public R remove( Long id){
		if(changeService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("system:change:batchRemove")
	public R remove(@RequestParam("ids[]") Long[] ids){
		changeService.batchRemove(ids);
		return R.ok();
	}


	@GetMapping("/changeHoutai")
	@RequiresPermissions("system:change:change")
	String ChangeHoutai() {
		return "system/change/changeHoutai";
	}

	@ResponseBody
	@GetMapping("/listHoutai")
	@RequiresPermissions("system:change:change")
	@Log("后台现金转积分记录列表")
	public PageUtils listHoutai(@RequestParam Map<String, Object> params) {
		//查询列表数据
		Query query = new Query(params);
		List<Map<String,Object>> changeList = changeService.listHoutai(query);
		int total = changeService.countHoutai(query);
		PageUtils pageUtils = new PageUtils(changeList, total);
		return pageUtils;
	}

}
