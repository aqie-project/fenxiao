package com.pojoin.system.service;

import com.pojoin.system.domain.OrderDO;
import com.pojoin.system.domain.UserDO;

import java.util.List;
import java.util.Map;

/**
 * 充值表
 * 
 * @author pojoin
 * @email 
 * @date 2020-06-30 20:02:39
 */
public interface OrderService {
	
	OrderDO get(Integer id);
	
	List<OrderDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(OrderDO order);
	
	int update(OrderDO order);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	void merge(UserDO newUser);

	Double selectTodayMoney();
}
