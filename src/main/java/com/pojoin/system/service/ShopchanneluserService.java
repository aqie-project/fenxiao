package com.pojoin.system.service;

import com.pojoin.system.domain.ShopchanneluserDO;

import java.util.List;
import java.util.Map;

/**
 * 渠道商表
 * 
 * @author pojoin
 * @email 
 * @date 2020-07-15 17:59:35
 */
public interface ShopchanneluserService {
	
	ShopchanneluserDO get(Integer id);
	
	List<ShopchanneluserDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ShopchanneluserDO shopchanneluser);
	
	int update(ShopchanneluserDO shopchanneluser);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}
